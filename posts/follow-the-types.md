[follow-the-types]
tags = ["blog", "lean"]
date = "2022-11-09"
title = "Follow The Types"

One of the "sweet spots" of using strong type systems is when your types are so written that your problem has only one
well-typed solution, and that is the correct one.

Consider the following formalization of a category:

```lean
class Category (Obj : Type) where
  arrow : Obj -> Obj -> Type
  id : ∀ a : Obj, arrow a a
  comp : ∀ {a b c : Obj}, arrow b c -> arrow a b -> arrow a c
  assoc : ∀ {a b c d : Obj}, ∀ k : arrow c d, ∀ g : arrow b c, ∀ f : arrow a b,
    comp k (comp g f) = comp (comp k g) f
  l_unit: ∀ {a b : Obj}, ∀ f : arrow a b,
    comp (id b) f = f
  r_unit: ∀ {b c : Obj}, ∀ g : arrow b c,
    comp g (id b) = g
    
infix:51 " ->> " => Category.arrow

prefix:91 "𝟙 " => Category.id

infixr:90 " ⊚ " => Category.comp
```

Given two categories, we can construct a new one considered as a product. MacLane defines the product's objects and
arrows as follows:

> An object of $B \times C$ is a pair $<b, c>$ of objects $b$ of $B$ and $c$ of $C$; an arrow $<b, c> \to <b', c'>$ of
> $B \times C$ is a pair $<f, g>$ of arrows $f : b \to b'$ and $g : c \to c'$ ...

However, in order to define the product category in Lean, we need to provide more data than just descriptions of the
objects and arrows.

```lean
instance [Category C] [Category B] : Category (C × B) where
  arrow := fun (c, b) (c', b') => (c ->> c') × (b ->> b')
  id := sorry -- (a : C × B) → ...
```

Given the type of objects and arrows, we can use `sorry` to iteratively fill in the rest of the instance until we're
done. Checking the type of this hole, we see it's a function (of course, we could also look at the type of `id` in the
class definition).

```lean
  id := fun (c, b) => sorry -- (c ->> c) × (b ->> b)
```

Checking again, the new type is a little intimidating, but we can figure out what it reduces to. And since our options
for creating arrows given nothing but an object are limited, we have no choice:

```lean
  id := fun (c, b) => (𝟙 c, 𝟙 b)
```

We can use the same method for composition.

```lean
  comp := sorry
```

```
  id := fun (c, b) => (𝟙 c, 𝟙 b)
  comp := fun (bc₁, bc₂) (ab₁, ab₂) => (bc₁ ⊚ ab₁, bc₂ ⊚ ab₂)
  assoc := by
    intro intro _ _ _ (k₁, k₂) (g₁, g₂) (f₁, f₂)
    simp
    constructor
    apply assoc
    apply assoc
  l_unit := by
    intro _ _ (f₁, f₂)
    simp
    constructor
    apply l_unit
    apply l_unit
  r_unit := by
    intro _ _ (f₁, f₂)
    simp
    constructor
    apply r_unit
    apply r_unit
```
