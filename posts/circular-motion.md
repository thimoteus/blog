---
title = "Circular Motion"
date = "2024-06-01"
tags = ["physics"]
summarize = false
template = "note.html"
---

Suppose a particle is in circular motion with constant speed.

$$
\begin{align*}
r^2 &= \v r\cdot \v r = \text{const} \\
v^2 &= \v v\cdot \v v = \text{const}
\end{align*}
$$

Differentiating these, we get

$$
\begin{align*}
2\v r\cdot\diff{\v r}t = 0 &\implies \v r\cdot\v v = 0 \\
2\v v\cdot \diff{\v v}t = 0 &\implies \v v\cdot\v a = 0 \\
\end{align*}
$$

So position is perpendicular to velocity which is perpendicular to acceleration.
Then either position and acceleration are parallel or antiparallel.
We can differentiate again to get

$$
\diff{}t(\v r\cdot\v v) = v^2 + \v r\cdot\v a = 0
$$

By definition of the dot product, we have

$$ \v r\cdot\v a = ra\cos\theta = -v^2 $$

Therefore $\cos\theta < 0$ and since $\v r, \v a$ are either parallel or antiparallel, $\theta = \pi$.
Thus

$$ a = \frac{v^2}r $$