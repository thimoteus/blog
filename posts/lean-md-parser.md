---
tags = ["lean"]
date = "2024-06-27"
title = "Making a Markdown Parser in Lean"
summarize = false
---

Types:

```lean
inductive MD where
  | text : String -> MD
  | bold : Array MD -> MD
  | italic : Array MD -> MD
  | inlineMath : String -> MD
  | displayMath : String -> MD
  | link (text : Array MD) (url : String) : MD
  | image (text : Array MD) (url : String) : MD
  | header (level : Fin 6) : Array MD -> MD
  | inlineCode (lang : Option String) : String -> MD
  | blockCode (lang : Option String) : String -> MD
  | newline : MD
  deriving Repr, BEq

namespace MD

inductive TopLevel where
  | paragraph : Array MD -> TopLevel
  | header : Fin 6 -> Array MD -> TopLevel
  | displayMath : String -> TopLevel
  | blockCode : Option String -> String -> TopLevel
  | orderedList : Array (Array MD) -> TopLevel
  | unorderedList : Array (Array MD) -> TopLevel
  | quote : Array (Array MD) -> TopLevel
  | rule : TopLevel
  deriving Repr

def mapChildren (f : Array MD -> Array MD) : MD -> MD
  | .text t => .text t
  | .bold mds => .bold (f mds)
  | .italic mds => .italic (f mds)
  | .inlineMath t => .inlineMath t
  | .displayMath t => .displayMath t
  | .link mds t => .link (f mds) t
  | .image mds t => .image (f mds) t
  | .header lvl mds => .header lvl (f mds)
  | .inlineCode lang? t => .inlineCode lang? t
  | .blockCode lang? t => .blockCode lang? t
  | .newline => .newline

end MD
```

Lexer:
```lean
inductive Cover where
  | open
  | close
  deriving Repr, BEq

inductive Token where
  | text : String -> Token
  | dollar : Nat -> Token
  | pound : Nat -> Token
  | star : Nat -> Token
  | tick : Nat -> Token
  | brace : Cover -> Token
  | bracket : Cover -> Token
  | parens : Cover -> Token
  | newline : Token
  | underscore : Token
  deriving Repr, BEq

instance : ToString Token where
  toString := fun t =>
    match t with
    | .text t => t
    | .dollar n => "".pushn '$' n
    | .pound n => "".pushn '#' n
    | .star n => "".pushn '*' n
    | .tick n => "".pushn '`' n
    | .brace .close => "}"
    | .brace .open => "{"
    | .bracket .close => "]"
    | .bracket .open => "["
    | .parens .close => ")"
    | .parens .open => "("
    | .newline => "\n"
    | .underscore => "_"

def Token.length : Token -> Nat
  | .text t => t.length
  | .dollar n => n
  | .pound n => n
  | .star n => n
  | .tick n => n
  | .brace .close
  | .brace .open
  | .bracket .close
  | .bracket .open
  | .parens .close
  | .parens .open
  | .newline
  | .underscore => 1

private structure State where
  tokens : Array Token
  currToken : Option Token
  pos : String.Pos

private def initialTokenState : State :=
  { tokens:= #[]
  , currToken := none
  , pos := 0
  }

private def Char.toToken : Char -> Token
  | '$' => .dollar 1
  | '`' => .tick 1
  | '#' => .pound 1
  | '*' => .star 1
  | '{' => .brace .open
  | '}' => .brace .close
  | '[' => .bracket .open
  | ']' => .bracket .close
  | '(' => .parens .open
  | ')' => .parens .close
  | '_' => .underscore
  | '\n' | '\r' => .newline
  | c => .text c.toString

private def nextToken (t : Token) (c : Char) : Token :=
  match t, c.toToken with
  | .dollar n, .dollar 1 => .dollar (n + 1)
  | .tick n, .tick 1 => .tick (n + 1)
  | .pound n, .pound 1 => .pound (n + 1)
  | .star n, .star 1 => .star (n + 1)
  | .text t₁, .text t₂ => .text (t₁ ++ t₂)
  | _, t => t

private def adjustTokens (old : Option Token) (new : Token) : Option Token :=
  match old, new with
  | none, _
  | some (.dollar _), .dollar _
  | some (.tick _), .tick _
  | some (.pound _), .pound _
  | some (.star _), .star _
  | some (.text _), .text _ => none
  | some t, _ => some t

def lex (inp : String) : Array Token := Id.run do
  let mut st := initialTokenState;
  while keepGoing : ¬inp.atEnd st.pos do
    let c := inp.get' st.pos keepGoing;
    let oldToken := st.currToken;
    let newToken :=
      match oldToken with
      | none => c.toToken
      | some t => nextToken t c
    let newTokens :=
      match adjustTokens st.currToken newToken with
      | some t => st.tokens.push t
      | _ => st.tokens
    st :=
      { tokens := newTokens
      , currToken := newToken
      , pos := inp.next' st.pos keepGoing
      };
  if let some t := st.currToken then
    st := { st with tokens := st.tokens.push t };
  return st.tokens;
```

Parser:

```lean
private inductive Error where
  | nonEmptyContext : String -> Error
  deriving Repr

private structure Position where
  line : Nat
  col : Nat

private abbrev Parser := StateT Position (Except Error)

private inductive LinkText where
  | unfinished : Array Token -> LinkText
  | finished : Array Token -> LinkText

private inductive Context where
  | bold : Array Token -> Context
  | italicStar : Array Token -> Context
  | italicUS : Array Token -> Context
  | link : LinkText -> Array Token -> Context
  | header : Nat -> Array Token -> Context
  | inlineMath : Array Token -> Context
  | displayMath : Array Token -> Context
  | inlineCode : Array Token -> Context
  | blockCode : Option String -> Array Token -> Context

private theorem min_fin (n m : Nat) : 0 < n -> Nat.min (n - 1) m < n := by
  intro n_pos
  unfold Nat.min
  have : min (n - 1) m = if n - 1 <= m then n - 1 else m := Nat.min_def
  rw [this]
  split
  · case isTrue =>
      cases n
      · contradiction
      · simp
  · case isFalse asm =>
      rw [Nat.not_le] at asm
      have m_succ_lt_n : m + 1 < n := by apply Nat.add_lt_of_lt_sub asm
      have m_lt_m_succ : m < m + 1 := Nat.lt_succ_self m
      exact Nat.lt_trans m_lt_m_succ m_succ_lt_n

private def fin6 (n : Nat) : Fin 6 :=
  let n' := min 5 n
  have : n' < 6 := by
    apply min_fin 6 n
    simp
  ⟨n', this⟩

private def foldString (xs : Array String) : String :=
  xs.foldl (· ++ ·) ""

private def shortText (str : String) : String :=
  if str.length > 10 then
    str.take 10 ++ "..."
  else
    str

private def parseAux
  (fuel : Nat)
  (tkns : Array Token) : Parser (Array MD) :=
  match fuel with
  | 0 => pure #[]
  | thrust + 1 => do
    let mut idx := 0;
    let mut context? : Option (Position × Context) := none;
    let mut result := #[];
    while inBounds : idx < tkns.size do
      let t := tkns[idx];
      let pos <- StateT.get;
      if let .newline := t then
        StateT.set { line := pos.line + 1, col := 1 };
      if let some context := context? then
        match context with
        | (pos, .bold ts) =>
          if let .star 2 := t then
            let res <- parseAux thrust ts;
            result := result.push <| .bold res;
            context? := none;
            idx := idx + 1;
          else
            context? := some (pos, .bold <| ts.push t);
            idx := idx + 1;
        | (pos, .italicStar ts) =>
          match t with
          | .star 1 =>
            let res <- parseAux thrust ts;
            result := result.push <| .italic res;
            context? := none;
            idx := idx + 1;
          | _ =>
            context? := some (pos, .italicStar <| ts.push t);
            idx := idx + 1;
        | (pos, .italicUS ts) =>
          match t with
          | .underscore =>
            let res <- parseAux thrust ts;
            result := result.push <| .italic res;
            context? := none;
            idx := idx + 1;
          | _ =>
            context? := some (pos, .italicUS <| ts.push t);
            idx := idx + 1;
        | (pos, .link ts₁ ts₂) =>
          match ts₁ with
          | .unfinished ts =>
            match t, tkns.get? (idx + 1) with
            | .bracket .close, some <| .parens .open =>
              context? := some (pos, .link (.finished ts) #[]);
              idx := idx + 2;
            | .bracket .close, _ =>
              let inner <- parseAux thrust ts;
              result := result.push (.text "[") ++ inner.push (.text "]");
              context? := none;
              idx := idx + 1;
            | _, _ =>
              context? := some (pos, .link (.unfinished <| ts.push t) ts₂);
              idx := idx + 1;
          | .finished ts =>
            if let .parens .close := t then
              let linkText <- parseAux thrust ts;
              let linkAddress := ts₂.map toString |> foldString;
              let link := .link linkText linkAddress;
              result := result.push link;
              context? := none;
              idx := idx + 1;
            else
              context? := some (pos, .link ts₁ <| ts₂.push t);
              idx := idx + 1;
        | (pos, .header n ts) =>
          match t with
          | .newline =>
            let mds <- parseAux thrust ts;
            let lvl := fin6 n;
            result := result.push <| .header lvl <| mds.push .newline;
            context? := none;
            idx := idx + 1;
          | _ =>
            if let none := tkns.get? <| idx + 1 then
              let lvl := fin6 n;
              let mds <- parseAux thrust <| ts.push t;
              result := result.push <| .header lvl mds;
              context? := none;
              break;
            else
              context? := some (pos, .header n <| ts.push t);
              idx := idx + 1;
        | (pos, .inlineMath ts) =>
          if let .dollar 1 := t then
            let text := ts.map toString |> foldString;
            result := result.push <| .inlineMath text;
            context? := none;
            idx := idx + 1;
          else
            context? := some (pos, .inlineMath <| ts.push t);
            idx := idx + 1;
        | (pos, .displayMath ts) =>
          if let .dollar 2 := t then
            let text := ts.map toString |> foldString;
            result := result.push <| .displayMath text;
            context? := none;
            idx := idx + 1;
          else
            context? := some (pos, .displayMath <| ts.push t);
            idx := idx + 1;
        | (pos, .inlineCode ts) =>
          if let .tick 1 := t then
            let text := ts.map toString |> foldString;
            let langTkns? := tkns.extract (idx + 1) (idx + 4);
            if let #[.brace .open, .text lang, .brace .close] := langTkns? then
              result := result.push <| .inlineCode (some lang) text;
              context? := none;
              idx := idx + 4;
            else
              result := result.push <| .inlineCode none text;
              context? := none;
              idx := idx + 1;
          else
            context? := some (pos, .inlineCode <| ts.push t);
            idx := idx + 1;
        | (pos, .blockCode lang? ts) =>
          if ts.isEmpty && lang?.isNone then
            match t, tkns.get? <| idx + 1 with
            | .text lang, some .newline =>
              context? := some (pos, .blockCode lang ts);
              idx := idx + 2;
            | .newline, some (.tick 3) =>
              context? := none;
              result := result.push <| .blockCode none "";
              idx := idx + 2;
            | .newline, some t' =>
              context? := some (pos, .blockCode none #[t'])
              idx := idx + 2;
            | _, _ =>
              context? := some (pos, .blockCode none #[t]);
              idx := idx + 1;
          else if t == .newline && some (.tick 3) == tkns.get? (idx + 1) then
            let text := ts.map toString |> foldString;
            result := result.push <| .blockCode lang? text;
            context? := none;
            idx := idx + 2;
          else if let .tick 3 := t then
            let text := ts.map toString |> foldString;
            result := result.push <| .blockCode lang? text;
            context? := none;
            idx := idx + 1;
          else
            context? := some (pos, .blockCode lang? <| ts.push t);
            idx := idx + 1;
      else
        match t with
        | .dollar 1 =>
          context? := some (pos, .inlineMath #[]);
        | .dollar 2 =>
          context? := some (pos, .displayMath #[]);
        | .dollar n =>
          result := result.push <| .text <| toString <| Token.dollar n;
        | .tick 1 =>
          context? := some (pos, .inlineCode #[]);
        | .tick 3 =>
          context? := some (pos, .blockCode none #[]);
        | .tick n =>
          result := result.push <| .text <| toString <| Token.tick n;
        | .pound n =>
          context? := some (pos, .header (min n 6) #[]);
        | .star 2 =>
          context? := some (pos, .bold #[]);
        | .star 1 =>
          context? := some (pos, .italicStar #[]);
        | .underscore =>
          context? := some (pos, .italicUS #[]);
        | .star n =>
          result := result.push <| .text <| toString <| Token.star n;
        | .brace cv =>
          result := result.push <| .text <| toString <| Token.brace cv;
        | .parens cv =>
          result := result.push <| .text <| toString <| Token.parens cv;
        | .text t =>
          result := result.push <| .text t;
        | .bracket .open =>
          context? := some (pos, .link (.unfinished #[]) #[]);
        | .bracket .close =>
          result := result.push <| .text <| toString t;
        | .newline =>
          result := result.push .newline;
        if t != .newline then
          StateT.set { pos with col := pos.col + t.length };
        idx := idx + 1;

    if let some (pos, context) := context? then
      let (tag, text) := match context with
        | .bold ts => ("bold", ts.map toString |> foldString)
        | .italicUS ts | .italicStar ts => ("italics", ts.map toString |> foldString)
        | .header _ ts => ("header", ts.map toString |> foldString)
        | .link ts _ =>
          let text := match ts with
            | .unfinished ts' | .finished ts' => ts'.map toString |> foldString
          ("link", text)
        | .blockCode _ ts => ("block code", ts.map toString |> foldString)
        | .inlineCode ts => ("inline code", ts.map toString |> foldString)
        | .displayMath ts => ("display math", ts.map toString |> foldString)
        | .inlineMath ts => ("inline math", ts.map toString |> foldString)
      let msg := s!"expecting closer for {tag} after line {pos.line}, col {pos.col}: {shortText text}";
      throw <| .nonEmptyContext msg;
    return result;

private def parseTokens (fuel : Nat) (inp : Array Token) : Except String (Array MD) :=
  let res := parseAux fuel inp |>.run' { line := 1, col := 1 }
  match res with
  | .ok mds => pure mds
  | .error <| .nonEmptyContext msg => .error msg

namespace Rewrites

private def joinText (fuel : Nat) (inp : Array MD) : Array MD :=
  match fuel with
  | 0 => #[]
  | thrust + 1 => Id.run do
    let mut idx := 0;
    let mut result := #[];
    let mut context? := none;
    while inBounds : idx < inp.size do
      match inp[idx] with
      | .text t =>
        if let some context := context? then
          context? := context ++ t;
        else
          context? := t;
        idx := idx + 1;
      | el =>
        let el := el.mapChildren <| joinText thrust;
        if let some context := context? then
          result := result ++ #[.text context, el];
          context? := none;
        else
          result := result.push el;
        idx := idx + 1;
    if let some context := context? then
      result := result.push <| .text context;
    return result;

private def transformImage (md₁ md₂ : MD) : MD × MD :=
  match md₁, md₂ with
  | .text t, .link mds₁ mds₂ =>
    if t.endsWith "!" then
      (.text <| t.dropRight 1, .image mds₁ mds₂)
    else
      (md₁, md₂)
  | _, _ =>
    (md₁, md₂)

private def setTwo (arr : Array a) (x y : a × Fin arr.size) : Array a :=
  arr.set x.snd x.fst |>.set ⟨y.snd.val, by simp [*]⟩ y.fst

private def setErase (arr : Array a) (x : a) (i j : Fin arr.size) : Array a :=
  arr.set i x |>.feraseIdx ⟨j, by simp [*]⟩

private def offsetIdx (ubd : i < l) (lbd : j <= i) : Fin l :=
  ⟨j, Nat.lt_of_le_of_lt lbd ubd⟩
example : i - 1 <= i := Nat.pred_le i
example : i - 2 <= i := Nat.sub_le i 2

private def images (fuel : Nat) (inp : Array MD) : Array MD :=
  match fuel with
  | 0 => #[]
  | thrust + 1 => Id.run do
    let mut idx := 1;
    let mut result := inp;
    while inBounds : idx < result.size do
      let i₀ := offsetIdx inBounds <| Nat.pred_le idx;
      let i₁ := ⟨idx, inBounds⟩;
      let el₀ := result[i₀].mapChildren <| images thrust;
      let el₁ := result[i₁].mapChildren <| images thrust;
      let (el₀, el₁) := transformImage el₀ el₁
      result := if let .text "" := el₀ then
        setErase result el₁ i₁ i₀
      else
        setTwo result (el₀, i₀) (el₁, i₁);
      idx := idx + 1;
    return result;

private def dropNewlines (inp : Array MD) : Array MD := Id.run do
  let mut res := #[];
  let mut idx := 0;
  while inBounds : idx < inp.size do
    if let .newline := inp[idx] then
      idx := idx + 1;
    else
      res := inp.extract idx inp.size;
      break;
  return res;

private def dropNewlinesBack : Array MD -> Array MD :=
  Array.reverse ∘ dropNewlines ∘ Array.reverse

private def trimNewlines : Array MD -> Array MD :=
  dropNewlinesBack ∘ dropNewlines

private def paragraphs (inp : Array MD) : Array MD.TopLevel := Id.run do
  let mut idx := 0;
  let mut result := #[];
  let mut context? := none;
  while inBounds : idx < inp.size do
    let md := inp[idx];
    match md with
    | .newline =>
      if let some context := context? then
        match inp.get? (idx + 1) with
        | some .newline
        | none =>
          result := result.push <| .paragraph context;
          context? := none;
          idx := idx + 2;
        | _ =>
          context? := context.push md;
          idx := idx + 1;
      else
        if let some .newline := inp.get? (idx + 1) then
          context? := some #[];
          idx := idx + 2;
        else
          idx := idx + 1;
    | .header lvl mds =>
      if let some context := context? then
        result := result ++ #[.paragraph context, .header lvl mds];
        context? := none;
        idx := idx + 1;
      else
        result := result.push <| .header lvl mds;
        idx := idx + 1;
    | _ =>
      if let some context := context? then
        context? := context.push md;
        idx := idx + 1;
      else
        context? := #[md];
        idx := idx + 1;

  if let some context := context? then
    result := result.push <| .paragraph context;

  return result;

def mapParagraphs (f : Array MD -> Array MD.TopLevel) (inp : Array MD.TopLevel) : Array MD.TopLevel :=
  let go acc
    | .paragraph mds => acc ++ f (trimNewlines mds)
    | tl => acc.push tl
  inp.foldl go #[]

private def rules : Array MD.TopLevel -> Array MD.TopLevel :=
  mapParagraphs fun mds => Id.run do
    if let #[.text t] := mds then
      if t.length >= 3 && t.all (· = '-') then
        return #[.rule];
    return #[.paragraph mds];

private def blockCodeAndDisplayMath : Array MD.TopLevel -> Array MD.TopLevel :=
  mapParagraphs fun mds => Id.run do
    let mut res := #[];
    let mut context := #[];
    for md in mds do
      match md with
      | .blockCode lang? children =>
        if !context.isEmpty then
          res := res.push (.paragraph context) |>.push (.blockCode lang? children);
          context := #[];
        else
          res := res.push <| .blockCode lang? children;
      | .displayMath children =>
        if !context.isEmpty then
          res := res.push (.paragraph context) |>.push (.displayMath children);
          context := #[];
        else
          res := res.push <| .displayMath children;
      | _ =>
        context := context.push md;
    if !context.isEmpty then
      res := res.push <| .paragraph context;
    return res;

private def toplvlFromParagraphLines
  (f? : String -> Option String)
  (mk : Array (Array MD) -> MD.TopLevel)
  : Array MD.TopLevel -> Array MD.TopLevel :=
  mapParagraphs fun mds => Id.run do
    let mut idx := 0;
    let mut result := #[];
    while inBounds : idx < mds.size do
      match mds[idx] with
      | .text t =>
        if let some l := f? t then
          let tail := mds.extract (idx + 1) mds.size |>.takeWhile (· != .newline);
          result := result.push <| #[.text l] ++ tail;
          idx := idx + tail.size + 2;
        else
          return #[.paragraph mds];
      | _ =>
        return #[.paragraph mds];
    return #[mk result];

private def quoteLine? (str : String) : Option String := do
  if str.front == '>' then
    return str.drop 1;
  failure;

private def quotes : Array MD.TopLevel -> Array MD.TopLevel :=
  toplvlFromParagraphLines quoteLine? .quote

private def isOrderedLine (str : String) : Bool :=
  str.takeWhile (· != '.') |>.all (·.isDigit)

private def orderedLine? (str : String) : Option String := do
  if isOrderedLine str then
    return str.dropWhile (· != '.') |>.drop 1;
  failure;

private def orderedLists : Array MD.TopLevel -> Array MD.TopLevel :=
  toplvlFromParagraphLines orderedLine? .orderedList

private def unorderedLine? (str : String) : Option String := do
  if str.front == '-' then
    return str.drop 1;
  failure;

private def unorderedLists : Array MD.TopLevel -> Array MD.TopLevel :=
  toplvlFromParagraphLines unorderedLine? .unorderedList

end Rewrites

private def rewrites (fuel : Nat) : Array MD -> Array MD.TopLevel :=
  Rewrites.quotes
    ∘ Rewrites.unorderedLists
    ∘ Rewrites.orderedLists
    ∘ Rewrites.rules
    ∘ Rewrites.blockCodeAndDisplayMath
    ∘ Rewrites.paragraphs
    ∘ Rewrites.images fuel
    ∘ Rewrites.joinText fuel

def Markdown.parse (inp : String) (fuel : Nat := 1000) : Except String (Array MD.TopLevel) :=
  lex inp
    |> parseTokens fuel
    |>.map (rewrites fuel)
```