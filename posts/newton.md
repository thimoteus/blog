---
title = "Newtonian Mechanics"
date = "2024-06-01"
tags = ["physics"]
summarize = false
template = "note.html"
---

## Newton's 2nd Law in 2D Polar Coordinates

From [Polar Coordinates](polar) we have

$$
\begin{align*}
\diff{\uv r}{t} &= \omega \uv\phi \\
\diff{\uv\phi}{t} &= -\omega \uv r \\
\end{align*}
$$

Then differentiating the position vector:

$$
\begin{align*}
\diff{\v r}t &= \diff{}t(r\uv r) \\
&= \diff rt\uv r + r\diff{\uv r}t \\
&= \dot r\uv r + r\omega\uv \phi \\
v_r &= \dot r\\
v_\phi &= r\omega
\end{align*}
$$

Differentiating again to get acceleration:

$$
\begin{align*}
\frac{d^2\v r}{dt^2} &= \diff{}t\diff{\v r}t \\
&= \diff{}t (\dot r \uv r + r\omega \uv \phi) \\
&= \left(\ddot r \uv r + \dot r\diff{\uv r}t\right) + \left(\dot r\omega \uv\phi + r\left(\dot\omega\uv\phi + \omega \diff{\uv\phi}t\right)\right) \\
&= (\ddot r\uv r + \dot r\omega\uv\phi) + (\dot r\omega\uv\phi + r(\dot\omega\uv\phi + \omega (-\omega\uv r))) \\
&= (\ddot r - \omega^2r)\uv r + (\dot r\omega + \dot r\omega + r\dot\omega)\uv\phi \\
&= (\ddot r - \omega^2r)\uv r + (r\dot\omega + 2\dot r\omega)\uv\phi \\
\v a &= (\ddot r - \omega^2r)\uv r + (r\alpha + 2\dot r\omega)\uv\phi
\end{align*}
$$

Therefore, in polar coordinates, 

$$
\begin{align*}
\v F &= m\v a \\
F_r &= m(\ddot r - \omega^2 r) \\
F_\phi &= m(r\alpha + 2\dot r \omega)
\end{align*}
$$
