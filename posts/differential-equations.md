---
date = "2024-06-01"
title = "Differential Equations"
tags = ["physics"]
summarize = false
template = "note.html"
---

## Linear First-Order Equations

$$ y' + Py = Q $$

where $P, Q$ are functions of $x$.

To solve this, first consider the simpler case where $Q = 0$.
Then

$$
\begin{gather*}
y' + Py = 0 \\
\diff yx = -Py
\end{gather*}
$$

This can now be separated

$$
\begin{align*}
\frac{dy}y &= -Pdx \\
\ln y &= -\int Pdx + C \\
y &= Ae^{-\int Pdx}, A = e^C \\
\end{align*}
$$

Setting $I = \int Pdx$ we can get

$$
\begin{align*}
\diff Ix &= P \\
y &= Ae^{-I} \\
ye^I &= A \\
\end{align*}
$$

Then we differentiate

$$
\begin{align*}
\diff{}x(ye^I) &= y'e^I + ye^I\diff Ix \\
&= y'e^I + ye^IP \\
&= e^I(y' + Py)
\end{align*}
$$

The term in parentheses is the LHS of the first, general equation.

$$ \diff{}x(ye^I) = e^I(y' + Py) = Qe^I $$

But both $Q$ and $e^I$ are functions of $x$ only, so we can integrate immediately

$$
\begin{align*}
ye^I &= \int Qe^Idx + C \\
y &= e^{-I}\int Qe^Idx + Ce^{-I}
\end{align*}
$$

This is the general solution.

## Other Methods

### Bernoulli Equation

The equation

$$ y' + Py = Qy^n $$

where $P, Q$ are functions of $x$ is called a Bernoulli Equation.

It isn't linear, but can be reduced to a linear equation by making the substitution

$$ z = y^{1 - n} $$

Then

$$ z' = (1-n)y^{-n}y' $$

We multiply the original equation by $(1-n)y^{-n}$ to get

$$
\begin{align*}
y'(1-n)y^{-n} + P(1 - n)y^{1-n} &= Q(1-n) \\
z' + (1-n)Pz &= (1-n)Q
\end{align*}
$$

and this is now a first-order equation.

### Exact Equations

The expression

$$ P(x,y)dx + Q(x,y)dy $$

is called an exact differential if

$$ \pdiff Py = \pdiff Qx $$

If this holds, then there is a function $F(x, y)$ such that

$$ dF \defeq \pdiff Fxdx + \pdiff Fydy = Pdx + Qdy $$

Sometimes you can turn an equation that isn't exact into an exact one by multiplying by some factor.
For example,

$$ xdy - ydx = 0 $$

is not exact, since

$$
\begin{align*}
P(x, y) &=  -y \\
Q(x, y) &= x \\
\pdiff Py &= -1 \\
\pdiff Qx &= 1 \\
\end{align*}
$$

However, we can multiply it by $x^{-2}$ to get

$$
\begin{align*}
\frac{1}xdy - \frac{y}{x^2}dx = 0
\end{align*}
$$

which is exact by setting $F(x, y) = \frac yx$:

$$
\begin{align*}
\pdiff Py &= \pdiff{}y\left(-\frac y{x^2}\right) &= -\frac 1{x^2} \\
\pdiff Qx &= \pdiff{}x\left(\frac 1x\right) &= -\frac 1{x^2} \\
\end{align*}
$$

The factor $x^{-2}$ is called an integrating factor.

### Homogeneous Equations

A homogeneous function of $x$ and $y$ of degree $n$ is of the form $x^nf(y/x)$, for example $x^3-xy^2 = x^3(1 - (\frac yx)^2)$.
A homogeneous equation is one of the form

$$ P(x,y)dx + Q(x,y)dy = 0 $$

where $P, Q$ are both homogeneous functions of the same degree.

Dividing one by the other will cancel out the $x^n$ factors, leaving $f(y/x)$.
So from the previous equation we can write

$$ y' = \diff yx = -\frac{P(x,y)}{Q(x,y)} = f\left(\frac yx\right) $$

Thus an equation is homogeneous if can be written in the form of $y' = f(y/x)$, which suggests solving by making the substitution

$$
\begin{align*}
u &= \frac yx \\
y &= ux
\end{align*}
$$

## Second Order Equations

### Linear Equations with Constant Coefficients and Zero RHS

Consider an equation of the form

$$ a_1\frac{d^2y}{d x^2} + a_2\diff yx + a_3y = 0 $$

Let $D = \diff{}x$. We can then rewrite this as

$$
\begin{align*}
a_1D^2y + a_2Dy + a_3y &= 0 \\
(a_1D^2 + a_2D + a_3)y &= 0
\end{align*}
$$

If the algebraic expression $a_1D^2 + a_2D + a_3 = (D - a)(D - b)$ then we can rewrite the original equation as

$$ (D - a)(D - b)y = 0 $$

Considering each factor in turn, we look at, without loss of generality

$$
\begin{align*}
(D - a)y &= 0 \\
\diff yx - ay &= 0 \\
\diff yx &= ay \\
\frac{dy}y &= adx \\
\ln y &= ax + C \\
y &= Ae^{ax}
\end{align*}
$$

Similarly, $(D - b)y = 0$ gives the solution $Be^{bx}$.
These are linearly independent, so the general solution is

$$ y = Ae^{ax} + Be^{bx} $$

Thus, when $a \neq b$ the general solution is given by the above.

When $a = b$ then we get

$$ (D - a)(D - a)y = 0 $$

One solution we already found, $y = Ae^{ax}$.
For the other, we use the substitution

$$ u = (D - a)y $$

Then we can rewrite

$$
\begin{align*}
(D - a)(D - a)y &= 0 \\
(D - a)u &= 0 \\
u &= Ae^{ax} \\
\end{align*}
$$

Then we eliminate $u$

$$
\begin{align*}
(D - a)y &= Ae^{ax} \\
y' - ay &= Ae^{ax} \\
\end{align*}
$$

This is a first-order equation we can solve to get

$$
\begin{align*}
P &= -a \\
Q &= Ae^{ax} \\
I &= \int Pdx = -ax \\
ye^I &= \int Qe^Idx \\
&= \int Ae^{ax}e^{-ax}dx \\
&= \int Adx \\
&= Ax + B
\end{align*}
$$

Therefore, the solution is

$$ y = e^{ax}(Ax + B) $$

This is the general solution, as it subsumes the known one.