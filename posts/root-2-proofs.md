---
tags = ["blog", "math"]
date = "2023-04-17"
title = "When LLMs Try to Write Proofs"
---

Whenever I get access to a new LLM, I always start by asking it to provide a proof of the irrationality of $\sqrt{2}$.
When there are errors, they can be subtle or hilariously obvious.

Here's a collection of some of them:

## Bing

> Euclid proved that √2 (the square root of 2) is an irrational number using a proof by contradiction. First Euclid
> assumed √2 was a rational number and then went on to show that in the form p/q it can always be simplified. But we
> can't go on simplifying an integer ratio forever, so there is a contradiction. So √2 must be an irrational number.

This isn't even a proof, it's just a summary of one. Given Bing's propensity to insult its users, I decided not to
press the issue.

## ChatGPT Take 1

```nohighlight
To prove that the square root of 2 is irrational, we can use a proof by contradiction.
This involves assuming that the square root of 2 is rational (i.e. can be expressed
as a ratio of two integers), and then showing that this assumption leads to a contradiction.

Here is how the proof works:

1. We assume that the square root of 2 is rational, and can be written as a ratio
of two integers, a and b, such that a/b is in lowest terms (i.e. a and b have no
common factors other than 1).
2. This means that a/b is equal to the square root of 2, which we can rewrite as
(a/b)^2 = 2.
3. We can then square both sides of the equation to get a^2/b^2 = 2.
4. Since a/b is in lowest terms, a^2 and b^2 must also be in lowest terms. This
means that a^2 and b^2 cannot both be even numbers, because if they were, they
would have a common factor of 2 and would not be in lowest terms.
5. Therefore, exactly one of a^2 and b^2 must be even, and the other must be odd.
Without loss of generality, let's say that a^2 is even and b^2 is odd.
6. Since a^2 is even, it can be written as 2c, where c is an integer. Substituting
this into the equation from step 3, we get 2c/b^2 = 2.
7. Dividing both sides of the equation by 2, we get c/b^2 = 1.
8. Since c/b^2 is equal to 1, c must be equal to b^2. But this means that c is an
odd number, because b^2 is odd.
9. However, this contradicts the fact that c is an integer, because an integer
cannot be both odd and even at the same time. Therefore, our assumption that the
square root of 2 is rational must be false.

This contradiction shows that the square root of 2 cannot be expressed as a ratio
of two integers, and is therefore irrational.
```

The problem here is fairly subtle, but it occurs in line 5. It correctly states that $a^2$ and $b^2$ can't both be even
numbers, but then erroneously says exactly one is odd and one is even. It misses the case when they're both odd.

## ChatGPT Take 2

```nohighlight
One proof of the irrationality of the square root of 2 is as follows:

Suppose for the sake of contradiction that the square root of 2 is rational, so
it can be expressed as a ratio of two integers a/b (where a and b are not both 0
and b≠0). Then, squaring both sides of this equation we get 2 = (a/b)^2 = a^2/b^2.

Since a^2 and b^2 are both integers, it follows that 2 is also a ratio of two integers,
which is a contradiction. Hence, the assumption that the square root of 2 is rational
leads to a contradiction, and therefore it must be irrational.
```

What needs to be said? I found this one to be the funniest by far!

## Bard

> Here is a proof by contradiction that the square root of 2 is irrational:
>
> Assume that $\sqrt{2}$ is rational. Then we can write it in the form $\sqrt{2} = \frac{p}{q}$, where $p$ and $q$ are
> integers and $q \neq 0$. We can also assume that $\frac{p}{q}$ is in lowest terms, meaning
> that $p$ and $q$ have no common factors other than 1.
>
> Squaring both sides of the equation $\sqrt{2} = \frac{p}{q}$, we get $2 = \frac{p^2}{q^2}$.
> Multiplying both sides by $q^2$, we get $2q^2 = p^2$.
>
> Since $2q^2$ is even, $p^2$ must also be even. This means that $p$ must be even, since the
> square of an odd number is always odd.
>
> Let $p = 2k$ for some integer $k$. Substituting this into the equation $2q^2 = p^2$, we get
> $2q^2 = (2k)^2 = 4k^2$. Dividing both sides by 2, we get $q^2 = 2k^2$.
>
> Since $q^2$ is even, $q$ must also be even.
>
> But this contradicts our assumption that $\frac{p}{q}$ is in lowest terms, since $p$ and $q$
> now have a common factor of 2.
>
> Therefore, our original assumption must have been wrong, and $\sqrt{2}$ must be irrational.

The only one I found acceptable!
