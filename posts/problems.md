## 4.1
By writing $\bold a \cdot \bold b$ in terms of components, derive the product rule for dot products.

$$
\begin{align*}
\frac d {dt}(\bold a \cdot \bold b) &= \frac d {dt}(a_xb_x + a_yb_y + a_zb_z) \\
&= \frac d {dt} (a_xb_x) + \frac d {dt} (a_yb_y) + \frac d {dt} (a_zb_z) \\
&= a_xb_x' + a_x'b_x + a_yb_y' + a_y'b_y + a_zb_z' + a_z'b_z \\
&= (a_xb_x' + a_yb_y' + a_zb_z') + (a_x'b_x + a_y'b_y + a_z'b_z) \\
&= \bold a \cdot \frac {d\bold b} {dt} + \frac {d\bold a} {dt} \cdot \bold b
\end{align*}
$$

## 1.43
Prove the unit vector of 2d polar coordinates is

$$ \hat{\bold r} = \hat{\bold x}\cos\phi + \bold{\hat y}\sin\phi $$ 

and find a corresponding expression for $\uv\phi$.

$$
\begin{align*}
r &= \sqrt{x^2 + y^2} \\
\phi &= \tan\inv\left(\frac yx\right) \\
x &= r\cos\phi \\
y &= r\sin\phi \\
\end{align*}
$$

By definition,

$$
\begin{align*}
\uv r &= \frac{\v r}{|\v r|} \\
&= \frac{\v r}{r} \\
&= \frac{x\uv x + y\uv y}{r} \\
&= \frac{r\cos\phi \uv x + r\sin\phi \uv y}{r} \\
&= \cos\phi \uv x + \sin\phi \uv y
\end{align*}
$$

For $\uv\phi$:

$$ \uv\phi = -\sin\phi\uv x + \cos\phi\uv y $$

by geometry.

Differentiating for $\uv r$:

$$
\begin{align*}
\diff{\uv r}{t} &= \diff{\uv r}{\phi}\diff{\phi}{t} \\
\diff{\uv r}{\phi} &= -\sin\phi\uv x + \cos\phi \uv y \\
&= \uv \phi \\
\diff{\uv r}{t} &= \omega\uv\phi \\
\omega &= \diff\phi t
\end{align*}
$$

Differentiating for $\uv \phi$:

$$
\begin{align*}
\diff{\uv\phi}{t} &= \diff{\uv\phi}{\phi}\diff{\phi}{t} \\
\diff{\uv\phi}{\phi} &= -\cos\phi\uv x - \sin\phi\uv y \\
&= -\uv r \\
\diff{\uv \phi}{t} &= -\omega\uv r
\end{align*}
$$
---
$$
\begin{align*}
\frac{yx'}{\sqrt{1+x'^2}} &= c_1 \\
y^2x'^2 &= c_2(1 + x'^2) \\
&= c_2 + c_2x'^2 \\
x'^2(y^2-c_2) &= c_2 \\
x' &= \frac{c_3}{\sqrt{y^2 - {c_3}^2}}
\end{align*}
$$

## 8.3

1. $y' + y = e^x$

This has the form $y' + Py = Q$, where $P = 1$ and $Q = e^x$.
Then the general solution is

$$ ye^I = \int Qe^Idx + C $$

So we calculate its terms

$$
\begin{gather*}
I \defeq \int Pdx = x \\
\int Qe^Idx = \int e^xe^xdx = \int e^{2x}dx
\end{gather*}
$$

Then we use integration by parts to get

$$
\begin{align*}
\int udv &= uv - \int vdu \\
u &= e^x \\
du &= e^xdx \\
dv &= e^xdx \\
v &= e^x \\
\int e^{2x}dx &= e^{2x} - \int e^{2x}dx \\
2\int e^{2x}dx &= e^{2x} \\
\int e^{2x} &= \frac 12 e^{2x}
\end{align*}
$$

Therefore we have

$$
\begin{align*}
ye^x &= \frac 12e^{2x} + c \\
y &= \frac 12e^x + ce^{-x}
\end{align*}
$$

2. $x^2y' + 3xy = 1$

Dividing everything by $x^2$ we get

$$ y' + 3x^{-1}y = x^{-2} $$

which has the appropriate form:

$$
\begin{align*}
P &= 3x^{-1} \\
Q &= x^{-2} \\
I &= \int Pdx \\
&= \int3x^{-1}dx \\
&= 3\ln x \\
\int Qe^Idx &= \int x^{-2}e^{3\ln x}dx \\
&= \int x^{-2}x^3dx \\
&= \int xdx = \frac 12x^2
\end{align*}
$$