---
title = "Hamiltonian Mechanics"
date = "2024-06-01"
tags = ["physics"]
summarize = false
template = "note.html"
---

## One Dimension

In one dimension, and in a conservative system with natural coordinates, the Lagrangian always takes the form

$$ \mathcal L = \mathcal L(q, \dot q) = T - U = \frac 12A(q)\dot q^2 - U(q) $$

Then the Hamiltonian is just

$$ \mathcal H = p\dot q - \mathcal L $$

Then the generalized momentum is

$$ p \defeq \pdiff{\mathcal L}{\dot q} = A(q)\dot q $$

From this we get

$$ p\dot q = A(q)\dot q^2 = 2T $$

Therefore

$$ \mathcal H = p\dot q - \mathcal L = 2T - (T - U) = T + U $$

So the Hamiltonian is the total energy.

Note that in the definition of generalized momentum, we can rewrite it so $\dot q$ is a function of $p$ and $q$

$$ \dot q = \frac p{A(q)} = \dot q(q, p) $$

In the Hamiltonian, we can replace all occurrences of $\dot q$ with $\dot q(q, p)$ which transforms it into a function of $q$ and $p$

$$ \mathcal H = p\dot q(q, p) - \mathcal L(q, \dot q(q, p)) $$

To get the equations of motion, we differentiate the Hamiltonian with respect to its two variables.

$$ \pdiff{\mathcal H}q = p\pdiff{\dot q}q - \left[\pdiff{\mathcal L}{q} + \pdiff{\mathcal L}{\dot q}\pdiff{\dot q}q\right] $$

where the term in brackets comes from the chain rule $\pdiff{\mathcal L}q\pdiff qq + \pdiff{\mathcal L}{\dot q}\pdiff{\dot q}q$.

However, the first and third terms are actually the same, so they cancel:

$$ \pdiff{\mathcal H}q = -\pdiff{\mathcal L}q = -\diff{}t\pdiff{\mathcal L}{\dot q} = -\dot p $$

where the second equality is the Euler-Lagrange equation.

The second equation of motion comes from the other partial derivative

$$ \pdiff{\mathcal H}p = \dot q + p\pdiff{\dot q}p - \pdiff{\mathcal L}{\dot q}\pdiff{\dot q}p = \dot q $$

Collecting both equations, in one dimension we have

$$
\begin{gather*}
\dot p = - \pdiff{\mathcal H}q \\
\dot q = \pdiff{\mathcal H}p
\end{gather*}
$$

### How to Solve

First, write down the Hamiltonian.
This is by definition $\mathcal H = \sum(p_i\dot q_i) - \mathcal L$, but it is sometimes easier to calculate $\mathcal H = T + U$ -- provided the generalized coordinates are natural (have no time-dependence when switching between these and Cartesian coordinates).
Then write down the generalized momentum, solve this equation for generalized velocity, then rewrite $\mathcal H$ as a function of position and momentum.
Then the equations of motions can be used.