---
title = "Lagrangian Mechanics"
date = "2024-06-01"
tags = ["physics"]
summarize = false
template = "note.html"
---

## Calculus of Variations and the Euler-Lagrange Equation

The problem is to find a function $y$ that makes the following integral stationary (have a critical point)

$$ \int_{x_1}^{x_2} F(x, y, y') dx $$

for a given $F$.
Any such $y(x)$ is called extremal regardless of whether it is a maximum, minimum or inflection point.

Since $y$ is optimal, a non-optimal function can be thought of as a deviation of $y$ in the following sense:

$$
\begin{align*}
Y(x) &= y(x) + \epsilon\eta(x) \nonumber \\
Y'(x) &= y'(x) + \epsilon\eta'(x) \nonumber \\
\diff Y\epsilon &= \eta(x) \tag{a} \\
\diff{Y'}\epsilon &= \eta'(x) \tag{b} \\
\end{align*}
$$

for some real parameter $\epsilon$ and "fudging" function $\eta$ subject to $\eta(x_1) = \eta(x_2) = 0$.

Then we consider the function

$$ I(\epsilon) = \int_{x_1}^{x_2} F(x, Y, Y')dx $$

and in particular, we want $\diff{I}\epsilon = 0$ when $\epsilon = 0$.

So we get

$$ \diff I\epsilon = \int_{x_1}^{x_2} \left( \pdiff FY \diff Y\epsilon + \pdiff F{Y'}\diff{Y'}\epsilon \right) dx $$

Substituting (a) and (b) in,

$$ \diff I\epsilon = \int_{x_1}^{x_2} \left( \pdiff FY\eta(x) + \pdiff F{Y'}\eta'(x) \right) dx $$

Note that when $\epsilon = 0$ then $Y = y$, therefore

$$ \left( \diff I\epsilon \right)_{\epsilon=0} = \int_{x_1}^{x_2} \left( \pdiff Fy\eta(x) + \pdiff F{y'}\eta'(x) \right)dx = 0 $$

Then assuming $y''$ is continuous we can use integration by parts on the second term:

$$
\begin{align*}
u &= \pdiff F{y'} \\
dv &= \eta'(x)dx \\
du &= \diff{}x\pdiff F{y'} \\
v &= \eta(x) \\
\int_{x_1}^{x_2} \pdiff F{y'}\eta'(x)dx &= \left( \pdiff F{y'}\eta(x) \right)_{x_1}^{x_2} - \int_{x_1}^{x_2}\eta(x)\diff{}x\pdiff F{y'}dx
\end{align*}
$$

Since $\eta(x_1) = \eta(x_2) = 0$ by assumption, the first term on the right vanishes.

$$ \left(\diff I\epsilon\right)_{\epsilon=0} = \int_{x_1}^{x_2} \left( \pdiff Fy - \diff{}x\pdiff F{y'} \right)\eta(x)dx = 0 $$

Because $\eta$ is arbitrary, the term in parentheses must be 0, for if it weren't, then we coul dchoose $\eta$ to make the whole integral nonzero, a contradiction.

The Euler-Lagrange equation is

$$ \pdiff Fy - \diff{}x \pdiff F{y'} = 0 $$

## Unconstrained Motion

For a particle moving unconstrained and subject to a conservative force, we have the kinetic energy

$$ T = \frac 12mv^2 = \frac 12m\dot r^2 = \frac 12m(\dot x^2 + \dot y^2 + \dot z^2) $$

and potential energy

$$ U = U(\v r) = U(x, y, z) $$

and then we define the Lagrangian

$$ \mathcal L = T - U $$

Note that the Lagrangian is a function of position and velocity, and that its derivatives are, wlog

$$
\begin{gather}
\pdiff{\mathcal L}{x} = -\pdiff Ux = F_x \\
\pdiff{\mathcal L}{\dot x} = \pdiff T{\dot x} = m\dot x = p_x \\
\end{gather}
$$

We can differentiate (2) wrt time and get

$$ \diff{}t \pdiff{\mathcal L}{\dot x} = \diff{}t \pdiff T{\dot x} = m\ddot x = \dot p_x = F_x $$

therefore

$$
\begin{gather*}
\pdiff{\mathcal L}x = \diff{}t \pdiff{\mathcal L}{\dot x} \\
\pdiff{\mathcal L}x - \diff{}t \pdiff{\mathcal L}{\dot x} = 0 \\
\end{gather*}
$$

Thus Newton's 2nd Law is equivalent to the Lagrangian equations

$$ \pdiff{\mathcal L}{q_i} - \diff{}t\pdiff{\mathcal L}{\dot{q_i}} = 0 $$

where $q_1 = x, q_2 = y, q_3 = z$.

Since the Lagrangian equations have the same form as the Euler-Lagrange equation, they imply the action integral $S = \int_{t_1}^{t_2} \mathcal L dt$ is stationary for the particle's path. This is known as Hamilton's Principle.

### Generalized Coordinates

We can use any other well-behaved coordinate system, so for the sake of generality, we will rewrite the Lagrangian and action integral as

$$
\begin{gather*}
\mathcal L = \mathcal L(q_1, q_2, q_3, \dot q_1, \dot q_2, \dot q_3) \\
S = \int_{t_1}^{t_2} \mathcal L(q_1, q_2, q_3, \dot q_1, \dot q_2, \dot q_3) dt
\end{gather*}
$$

When we write the Euler-Lagrange equation as

$$ \pdiff{\mathcal L}{q_i} = \diff{}t\pdiff{\mathcal L}{\dot{q_i}} $$

it is remeniscent of Newton's 2nd Law, $\v F = \diff{\v p}t$.
As such, we call the LHS the "generalized force" (though it may not be an actual force, depending on the coordinate system) and the RHS is the time derivative of the "generalized momentum".

If the generalized force is 0, then the generalized momentum is conserved.

Therefore, we use the following notation

$$
\begin{align*}
F_i &= \pdiff{\mathcal L}{q_i} \\
p_i &= \pdiff{\mathcal L}{\dot q_i} \\
F_i &= \diff{}t p_i \\
&= \dot{p}_i \\
\end{align*}
$$

### Angular Momentum and Torque

$$
\begin{align*}
T &= \frac 12 m v^2 \\
&= \frac 12 m (\v v \cdot \v v) \\
&= \frac 12 m (\dot r^2 + r^2\omega^2) \\
\end{align*}
$$

Therefore the Lagrangian is

$$
\begin{align*}
\mathcal L &= T - U \\
&= \frac 12m(\dot r^2 + r^2\omega^2) - U(r, \phi)
\end{align*}
$$

and the Euler-Lagrange equation for the radial component is

$$
\begin{align*}
\pdiff{\mathcal L}{r} &= \diff{}t\pdiff{\mathcal L}{\dot r} \\
mr\omega^2 - \pdiff Ur &= m\ddot r \\
-\pdiff Ur &= m(\ddot r - r\omega^2)
\end{align*}
$$

But the LHS is just $F_r$, so this equation gives the radial component of Newton's 2nd Law ($-r\omega^2$ is centripetal acceleration).

The Euler-Lagrange equation for the tangential component is

$$
\begin{align*}
\pdiff{\mathcal L}\phi &= \diff{}t\pdiff{\mathcal L}\omega \\
-\pdiff U\phi &= \diff{}t(mr^2\omega) \\
\end{align*}
$$

In order to interpret this, we relate the LHS to $\v F = -\nabla U$, which relies on

$$ \nabla U = \pdiff Ur\uv r + \frac 1r\pdiff U\phi\uv\phi $$

Therefore

$$
\begin{align*}
F_\phi &= -\frac 1r\pdiff U\phi \\
rF_\phi &= -\pdiff U\phi \\
rF_\phi &= \diff{}t(mr^2\omega)
\end{align*}
$$

In this last equation, the LHS is the definition of torque, and the RHS is the time-derivative of angular momentum.
Therefore, this can be rewritten as

$$ \tau = \diff{}t(I\omega) = \diff Lt $$

$$
\begin{align*}
\int_{t_1}^{t_2}m\v r'\cdot \v \epsilon'dt &= (m\v r'\cdot \v\epsilon)_{t_0}^{t_1} - \int_{t_0}^{t_1}m\v \epsilon \cdot \v r'' dt
\end{align*}
$$

## Solving Problems

A system is holonomic if the number of degrees of freedom is equal to the number of generalized coordinates needed to describe its configuration.

Assuming holonomicity and nonconstraint forces are conservative (or at least determine a potential energy):

1. Write down the kinetic and potential energies in an inertial reference frame, deriving the Lagrangian $\mathcal L = T - U$.
2. Choose a set of generalized coordinates $\{q_i | 0 \le i < n \}$ and find expressions for the original coordinates in terms of these new ones.
3. Rewrite the Lagrangian in generalized coordinates.
4. Write down the Lagrange equations, one for each coordinate.

## Conservation of Energy and the Hamiltonian

By the chain rule, we have

$$ \diff{}t \mathcal L(\v q, \dot{\v q}, t) = \sum_i \pdiff{\mathcal L}{q_i}\dot{q_i} + \sum_i \pdiff{\mathcal L}{\dot{q_i}}\ddot{q_i} + \pdiff{\mathcal L}t \tag{a} $$

The Euler-Lagrange eqution is

$$ \pdiff{\mathcal L}{q_i} = \diff{}t\pdiff{\mathcal L}{\dot{q_i}} = \diff{}tp_i = \dot{p_i} $$

and we can substitute it into (a) to get

$$ \diff{}t \mathcal L = \sum_i (\dot{p_i}\dot{q_i} + p_i \ddot{q_i}) + \pdiff{\mathcal L}t $$

We can then pull out a derivative to get

$$ \diff{}t \mathcal L = \diff{}t\sum_i(p_i\dot{q_i}) + \pdiff{\mathcal L}t $$

When the Lagrangian does not depend on time, $\pdiff{\mathcal L}t = 0$, and we can rewrite this to get

$$ \diff{}t\left( \sum_i(p_i\dot{q_i}) - \mathcal L \right) = 0 $$

The term in parentheses is called the Hamiltonian:

$$ \mathcal H = \sum_i(p_i\dot{q_i}) - \mathcal L \tag{H} $$

Thus, if the Lagrangian has no time-dependence, the Hamiltonian is conserved.

### The Hamiltonian is Just Total Energy

Provided our coordinate system is natural (the equivalence between it and Cartesian coordinates is not time-dependent)

$$ \v r_\alpha = \v r_\alpha(\v q) $$

we can differentiate this using the chain rule and get

$$ \diff{}t \v r_\alpha = \sum_i \pdiff{\v r_\alpha}{q_i}\dot{q_i} $$

Then we take the inner product with itself:

$$ \left( \diff{}t \v r_\alpha \right)^2 = \left(\sum_j \pdiff{\v r_\alpha}{q_j}\dot{q_j}\right) \cdot \left(\sum_k \pdiff{\v r_\alpha}{q_k}\dot{q_k}\right) $$

Define $A_{jk}$ as

$$ A_{jk}(\v q) = \sum_\alpha m_\alpha\left(\pdiff{\v r_\alpha}{q_j}\right)\cdot\left(\pdiff{\v r_\alpha}{q_k}\right) $$

Then we can rewrite the kinetic energy as

$$ T = \frac 12\sum_{j, k} A_{jk}\dot q_j \dot q_k $$

Now we'll derive the generalized momentum $p_i$ by differentiating this wrt $\dot q_i$

$$ p_i \defeq \pdiff{\mathcal L}{\dot q_i} = \pdiff T{\dot q_i} = \sum_j A_{ij}\dot q_j $$

Looking at equation (H), the Hamiltonian, we can then write

$$
\begin{align*}
\sum_ip_i \dot q_i &= \sum_i\left(\sum_j A_{ij}\dot q_j\right)\dot q_i \\
&= \sum_{i, j}A_{ij}\dot q_i\dot q_j \\
&= 2T
\end{align*}
$$

Therefore

$$
\begin{align*}
\mathcal H &\defeq \sum_i(p_i\dot q_i) - \mathcal L \\
&= 2T - \mathcal L \\
&= 2T - (T - U) \\
&= T + U
\end{align*}
$$

## Lagrange Multipliers

The general problem is to minimize or maximize a function $f(x, y)$ subject to a constraint $\phi(x, y) = c, c \in \mathbb R$.
Then you can think of $f$ as a function of one variable, and to find its stationary points, we set its derivatives to 0 (and since $\phi$ is constant)

$$
\begin{align*}
df &\defeq \pdiff fxdx + \pdiff fydy = 0 \\
d\phi &\defeq \pdiff\phi xdx + \pdiff\phi ydy = 0 \\
\end{align*}
$$

then we multiply $d\phi$ by an undetermined constant $\lambda$, the Lagrange multiplier, and add the two equations

$$ df + \lambda d\phi = \left(\pdiff fx + \lambda\pdiff\phi x\right)dx + \left(\pdiff fy + \lambda \pdiff\phi y\right)dy = 0 $$

Then choose $\lambda$ so the second term in parentheses is 0

$$ \pdiff fy + \lambda \pdiff\phi y = 0 \tag{a} $$

Then we also get, from $df + \lambda d\phi = 0$

$$ \pdiff fx + \lambda \pdiff\phi x = 0 \tag{b} $$

Then there are three equations (a, b, and $\phi$) and three unknowns ($x, y$ and $\lambda$).

This is equivalent to defining

$$ F(x, y) = f(x, y) + \lambda\phi(x, y) $$

and finding its stationary points by setting its partial derivatives equal to 0, and solving for the unknowns by using these equations and $\phi$.

### With the Lagrangian

Instead of taking the partial derivatives of a function $f$ and adding the modified constraint, we instead take the sum

$$ \pdiff{\mathcal L}x - \diff{}t\pdiff{\mathcal L}{\dot x} + \lambda\pdiff\phi x = 0 $$

and equivalent equations for other coordinates $y, z$ etc.
Then there are $n$ modified Lagrangian equations, and one constraint equation, with $n + 1$ unknowns.

As an examle, take the Lagrangian

$$ \mathcal L(x, \dot x, y, \dot y) = \frac 12m_1\dot x^2 + \frac 12 m_2\dot y^2 - U(x, y) $$

Inserting this into a modified Euler-Lagrange equation, we get

$$ -\pdiff Ux + \lambda\pdiff fx = m_1\ddot x $$

The first term on the left is the $x$-component of the nonconstraint force, while the RHS is the $x$-component of the total force, which is the sum of the constraint and nonconstraint forces.

Therefore

$$ \lambda\pdiff fx = F^{\text{cstr}}_x $$