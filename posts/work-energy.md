---
title = "Work and Energy"
date = "2024-06-01"
tags = ["physics"]
summarize = false
template = "note.html"
---

## Work-Energy Theorem
Defining kinetic energy as

$$ T = \frac 1 2 mv^2 = \frac 1 2 m(\bold v \cdot \bold v) $$

and differentiating with respect to time, we get

$$
\begin{align*}
\frac{dT}{dt} &= \frac 1 2 m \frac d {dt}(\bold v \cdot \bold v) \\
&= \frac 1 2 m (\bold v' \cdot \bold v + \bold v + \bold v') \\
&= m \bold v' \cdot \bold v \\
&= \bold F \cdot \bold v
\end{align*}
$$

Multiplying both sides by $dt$ and noticing that $\bold v dt = d\bold r$, we get

$$
\begin{align*}
dT &= \bold F \cdot d\bold r \\
\Delta T &= \int \bold F \cdot d \bold r
\end{align*}
$$

## Conservation of Mechanical Energy

A conservative force satisfies two conditions:

1. It depends only on the position vector $\bold r$.
2. For any two points, the work done by the force going from one to the other is independent of path taken.

If $A$ is any path from a point $a$ to a point $b$, then we define the work to go between them as the path integral

$$ W(a\to b) = \int_A\bold F\cdot d\bold r $$

Choose a reference point $\bold r_0$ where the potential energy $U$ will be defined to be 0.
At an arbitrary point, the potential energy is defined as

$$ U(\bold r) = -W(\bold r_0 \to \bold r) = - \int_{\bold r_0}^{\bold r} \bold F(\bold r') \cdot d\bold r' $$

Both conditions of conservativeness must be satisfied so $U$ this definition results in a unique function.

Then we can derive the difference in potential energy between two arbitrary points:

$$
\begin{align*}
W(\bold r_0 \to \bold r_2) &= W(\bold r_0 \to \bold r_1) + W(\bold r_1 \to \bold r_2) \\
W(\bold r_1 \to \bold r_2) &= W(\bold r_0 \to \bold r_2) - W(\bold r_0 \to \bold r_1) \\
&= -U(\bold r_2) + U(\bold r_1) \\
&= -(U(\bold r_2) - U(\bold r_1)) \\
&= -\Delta U
\end{align*}
$$

Combining this with the Work-Energy Theorem, we get

$$
\begin{align*}
\Delta T &= \int_{\bold r_1}^{\bold r_2} \bold F\cdot d\bold r \\
W(\bold r_1 \to \bold r_2) &= \int_{\bold r_1}^{\bold r_2}\bold F\cdot d\bold r \\
W(\bold r_1 \to \bold r_2) &= -\Delta U \\
\Delta T &= -\Delta U \\
\Delta T + \Delta U &= 0 \\
\Delta (T + U) &= 0 \\
\end{align*}
$$

where mechanical energy is $E = T + U$, which is conserved, hence "conservative force".

## Mechanical Energy and Nonconservative Forces

Split up the net force into conservative and nonconservative components:

$$ \bold F = \bold F_{cons} + \bold F_{nc} $$

Then by the Work-Energy Theorem, we have

$$ \Delta T = W = W_{cons} + W_{nc} $$

Notice that $W_{cons} = -\Delta U$ so that

$$
\begin{align*}
\Delta T + \Delta U &= W_{nc} \\
\Delta (T + U) &= W_{nc} \\
\Delta E &= W_{nc} \\
\end{align*}
$$

## Conservative Forces from Potential Energy

Given a conservative force $\bold F$ and potential $U$, the work done by the force over an infinitesimal distance is by definition

$$
\begin{align*}
W(\bold r \to \bold r + d\bold r) &= \bold F(\bold r) \cdot d\bold r \\
&= F_xdx + F_ydy + F_zdz
\end{align*}
\tag{a}
$$

but also

$$
\begin{align*}
W(\bold r \to \bold r + d\bold r) &= -dU \\
&= -(U(\bold r + d\bold r) - U(\bold r)) \\
&= -(U(x + dx, y + dy, z + dz) - U(x, y, z))
\end{align*}
$$

For single-valued functions, we have in the limit

$$ df = f(x + dx) - f(x) = \frac{df}{dx}dx $$

whereas for a multivalued function the equivalent definition is

$$
\begin{align*}
dU &= \frac{\partial U}{\partial x}dx + \frac{\partial U}{\partial y}dy + \frac{\partial U}{\partial z}dz \tag{b} \\
&= \nabla U \cdot d\bold r
\end{align*}
$$

Then we can substitute to get

$$ W(\bold r \to \bold r + d\bold r) = -\left[ \frac{\partial U}{\partial x}dx + \frac{\partial U}{\partial y}dy + \frac{\partial U}{\partial z}dz \right] \tag{c}$$

Equations (a) and (c) are valid for any displacement $d\bold r$, and in particular for $d\bold r$ parallel to each axis.
Then we get

$$
\begin{align*}
F_x &= -\frac{\partial U}{\partial x} \\
F_y &= -\frac{\partial U}{\partial y} \\
F_z &= -\frac{\partial U}{\partial z} \\
\end{align*}
$$

This can be rewritten as

$$
\begin{align*}
\nabla &= \frac{\partial}{\partial x}\bold{\hat x} + \frac{\partial}{\partial y}\bold{\hat y} + \frac{\partial}{\partial z}\bold{\hat z} \\
\bold F &= -\nabla U \\
\end{align*}
$$

Noticing that (b) can be written as $dU = \nabla U\cdot d\bold r$, we can replace $U$ with an arbitrary scalar-valued $f$, then the change in $f$ from a small displacement $d\bold r$ is

$$ df = \nabla f \cdot d\bold r $$

## Determining Conservativeness

Define the curl of a force as

$$
\begin{align*}
\nabla \times \v F &=
\begin{vmatrix}
\uv i & \uv j & \uv k \\
\pdiff{}x & \pdiff{}y & \pdiff{}z \\
F_x & F_y & F_z
\end{vmatrix} \\
&= \left( \pdiff{F_z}y - \pdiff{F_y}z \right)\uv i \\
&+ \left( \pdiff{F_x}z - \pdiff{F_z}x \right)\uv j \\
&+ \left( \pdiff{F_y}x - \pdiff{F_x}y \right)\uv k \\
\end{align*}
$$

Then $\nabla\times\v F = 0$ is equivalent to $\int\v F\cdot d\v r$ being path-independent.

This is because of the following: Suppose there is a function $W$ such that $\v F = \nabla W$.

$$
\begin{align*}
F_x &= \pdiff Wx \\
F_y &= \pdiff Wy \\
F_z &= \pdiff Wz \\
\end{align*}
$$

If, for example

$$ \frac{\partial^2W}{\partial xy} = \frac{\partial^2W}{\partial yx} $$

etc, (because the first and second order partial derivatives are continuous, for instance, from which the equality follows) then we get

$$ \pdiff{F_x}y = \frac{\partial^2W}{\partial yx} = \frac{\partial^2W}{\partial xy}= \pdiff{F_y}x $$

Therefore, each of the terms in the curl are 0.

Similarly, if the curl is 0, we can find a function $W$ such that $\v F = \nabla W$.

If such a function exists, then

$$
\begin{align*}
\v F\cdot d\v r &= \nabla W\cdot d\v r \\
&= \pdiff Wxdx + \pdiff Wydy + \pdiff Wzdz \\
&= dW \\
\int_A^B \v F\cdot d\v r &= \int_A^B dW \\
&= W(B) - W(A)
\end{align*}
$$

In other words, the line integral depends only on the value of $W$ at the endpoints, so it is path-independent.

## Time-Dependent Force and Nonconservation of Energy

Supposing $\bold F(\bold r, t)$ is a force that depends on position and time, but also $\nabla \times \bold F = 0$ so it's path independent.
Then we can still define a potential such that $\bold F = - \nabla U$ by

$$ U(\bold r, t) = -\int_{\bold r_0}^{\bold r} \bold F(\bold r, t) \cdot d\bold r $$

Defining the mechanical energy as before, $E = T + U$ we consider two points on the particle's path at times $t$ and $t + dt$.
Like before, we have

$$ dT = \frac{dT}{dt}dt = m\bold v' \cdot \bold v dt = \bold F \cdot d\bold r \tag{a}$$

but this time we also have

$$
\begin{align}
dU &= U(x + dx, y + dy, z + dz, t + dt) - U(x, y, z, t) \nonumber \\
&= \frac{\partial U}{\partial x}dx + \frac{\partial U}{\partial y}dy + \frac{\partial U}{\partial z}dz + \frac{\partial U}{\partial t}dt \nonumber \\
&= \nabla U \cdot d\bold r + \frac{\partial U}{\partial t}dt \nonumber \\
&= -\bold F \cdot d\bold r + \frac{\partial U}{\partial t} dt \tag{b}
\end{align}
$$

Then adding (b) to (a) we get

$$
d(T + U) = \frac{\partial U}{\partial t}dt
$$

so only when $U$ is independent of $t$ (in other words, $\frac{\partial U}{\partial t} = 0$) is mechanical energy conserved.

## Hooke's Law and Potential Energy

Consider a one-dimensional system specified by the coordinate $x$ with an equilibrium position $x = x_0$ which might as well be taken at the origin.
In a small neighborhood around the origin, we can take the Taylor series expansion of the potential:

$$ U(x) = U(0) + U'(0)x + \frac 12 U''(0)x^2 + ... $$

We can define $U(0) = 0$, and since the origin is an equilibrium point, $U'(0) = 0$.
Then $U''(0) > 0$, so we call this constant $k$ and we can approximate (for small values of $x$)

$$ U(x) \approx \frac 12 kx^2 $$
