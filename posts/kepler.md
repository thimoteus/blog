---
title = "Kepler's Laws of Planetary Motion"
date = "2024-06-01"
tags = ["physics"]
summarize = false
template = "note.html"
---

## Kepler's 2nd Law
> A line segment joining a planet and the Sun sweeps out equal areas during equal intervals of time.

Set the origin $O$ at the sun, and two pairs of points on the planet's elliptical orbit $(P, Q)$ and $(P', Q')$.
When a pair of points is close together, the area bounded by the origin and those points can be approximated by a triangle.
Let $dA$ denote the infinitesimal area of the triangle $OPQ$ and $dA'$ be the same for $OP'Q'$, and $dt$ (respectively $dt'$) be the infinitesimal time taken by the planet in going from $P$ (respectively $P'$) to $Q$ ($Q'$).

Then the length of the segment $PQ$ ($P'Q'$) is $d\bold r = \bold v dt$, and the length of the segment $OP$ is $\bold r$.

So the area of the triangle is given by

$$
\begin{align*}
dA &= \frac 1 2 |\bold r \times \bold v dt| \\
&= \frac1 2 |\bold r \times \frac{\bold p}{m} dt| \\
&= \frac{1}{2m}|\bold r \times \bold p dt| \\
\end{align*}
$$
which implies
$$
\begin{align*}
\frac{dA}{dt} &= \frac{1}{2m}|\bold r \times \bold p| \\
&= \frac 1 {2m} l
\end{align*}
$$

Since the angular momentum $l$ is conserved, this implies $\frac{dA}{dt}$ is constant.
