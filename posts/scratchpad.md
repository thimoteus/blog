title = "Scratchpad"
date = "2024-06-15"
summarize = false

$$ \v F = m\v a $$
test paragraph under math

```lean
private def setTwo (arr : Array a) (x y : a × Fin arr.size) : Array a :=
  arr.set x.snd x.fst |>.set ⟨y.snd.val, by simp [*]⟩ y.fst
```
test paragraph under code `inline code :: a -> a`{haskell}

> here's a
> quote

with nonquote stuff