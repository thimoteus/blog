---
title = "Polar Coordinates"
date = "2024-06-01"
tags = ["physics"]
summarize = false
template = "note.html"
---

## Polar Vectors and Derivatives

![The unit vectors in polar coordinates](images/polar-unit-vectors.png)

From geometry, we get that

$$
\begin{align*}
\uv r &= \cos\theta\uv i + \sin\theta\uv j \\
\uv \theta &= -\sin\theta\uv i + \cos\theta\uv j \\
\end{align*}
$$

Differentiating with respect to the angle, we get

$$
\begin{align*}
\diff{\uv r}\theta &= -\sin\theta\uv i + \cos\theta\uv j \\
&= \uv\theta \\
\diff{\uv \theta}\theta &= -\cos\theta\uv i - \sin\theta\uv j \\
&= -\uv r
\end{align*}
$$

Then we apply the chain rule to differentiate with respect to time:

$$
\begin{align*}
\diff{\uv r}t &= \diff{\uv r}\theta\diff\theta t \\
&= \omega\uv\theta \\
\diff{\uv\theta}t &= \diff{\uv\theta}\theta\diff\theta t \\
&= -\omega\uv r
\end{align*}
$$

Therefore, to get velocity we differentiate

$$
\begin{align*}
\v v &\defeq \diff{\v r}t \\
&=\diff{}t(r\uv r) \\
&= \diff rt\uv r + r\diff{\uv r}t \\
&= v\uv r + r\omega\uv\theta
\end{align*}
$$

Similarly we can derive acceleration

$$
\begin{align*}
\v a &\defeq \diff{\v v}t \\
&= \diff{}t(v\uv r) + \diff{}t(r\omega\uv\theta) \\
&= \left[a\uv r + v\diff{\uv r}t\right] + \left[v\omega\uv\theta + r\left(\diff{}t(\omega\uv\theta\right)\right] \\
&= \left[a\uv r + v\omega\uv\theta\right] + \left[v\omega\uv\theta + r\left(\alpha\uv\theta + \omega\diff{\uv\theta}t\right)\right] \\
&= \left[a\uv r + v\omega\uv\theta\right] + \left[v\omega\uv\theta + r\left(\alpha\uv\theta + \omega(-\omega\uv r)\right)\right] \\
&= \left(a - r\omega^2\right)\uv r + \left(2v\omega + r\alpha\right)\uv\theta
\end{align*}
$$

In the case of constant radius, for example in circular motion, $v = a = 0$ so we just get

$$ -r\omega^2\uv r + r\alpha\uv\theta $$

The first term can be rewritten as $-r\omega^2 = -r(v/r)^2 = -v^2/r$ and is the centripetal acceleration.
The second term is just the tangential acceleration $r\alpha = r(a/r) = a$.

When the radius is not constant, then $a$ is just linear acceleration, but $2v\omega$ is known as the Coriolis acceleration.

## Gradient in 2D

Noticing that $df = \nabla f \cdot d\v r$, we need an expression for $d\v r$.

The $\uv r$ component of $d\v r$ is just a small change $dr$ in the direction of $\v r$, while the $\uv \phi$ component is $rd\phi$.
Therefore

$$ d\v r = dr\uv r + rd\phi\uv\phi $$

Writing down $\nabla f$ in terms of its unknown components, we have

$$ \nabla f = (\nabla f)_r \uv r + (\nabla f)_\phi\uv\phi $$

Thus the dot product is

$$ \nabla f \cdot d\v r = (\nabla f)_rdr + (\nabla f)_\phi rd\phi $$

But we also have

$$ df = \pdiff frdr + \pdiff f\phi d\phi$$

So we can equate components and get

$$
\begin{align*}
(\nabla f)_r &= \pdiff fr \\
(\nabla f)_\phi &= \frac 1r \pdiff f\phi
\end{align*}
$$

Thus

$$ \nabla f = \pdiff fr\uv r + \frac 1r \pdiff f\phi\uv\phi $$