highlightJS := build-assets/highlight.js/
highlightLangs := haskell lean javascript bash

.PHONY : build repl build-highlighter style
build:
	pack build blog.ipkg

build-highlighter:
	cd $(highlightJS) && node tools/build.js -t browser $(highlightLangs) && cp build/highlight.min.js ../../public/assets/ && cp src/styles/base16/gruvbox-dark-hard.css ../../public/styles/

style:
	npm run css

repl:
	pack --with-ipkg blog.ipkg repl src/Main.idr

clean:
	rm -rf ./build

