module Main

import Data.List1
import Data.String

data HTMLNode : Type where [external]

%foreign """
javascript:lambda:(nothing, just, key) => {
  const url = new URL(document.location.href);
  const val = url.searchParams.get(key);
  if (val) {
    return just(val);
  }
  return nothing;
}
"""
prim__getQP : Maybe String -> (String -> Maybe String) -> String -> PrimIO (Maybe String)

getQP : String -> IO String
getQP key = map concat $ primIO $ prim__getQP Nothing Just key

%foreign """
javascript:lambda:(key) => {
  const htmlCol = document.getElementsByClassName(key);
  const htmlArr = [...htmlCol];
  return __prim_js2idris_array(htmlArr);
}
"""
prim__getElementsByClassName : String -> PrimIO (List HTMLNode)

getElementsByClassName : String -> IO (List HTMLNode)
getElementsByClassName key = primIO $ prim__getElementsByClassName key

%foreign "javascript:lambda:(str, el) => el.style.display = str"
prim__setDisplay : String -> HTMLNode -> PrimIO ()

setDisplay : String -> HTMLNode -> IO ()
setDisplay str el = primIO $ prim__setDisplay str el

hideNode : HTMLNode -> IO ()
hideNode = setDisplay "none"

showNode : HTMLNode -> IO ()
showNode = setDisplay ""

getTags : IO (List String)
getTags = do
  tags <- getQP "tags"
  let tags = filter (/= "") $ forget $ split (== ',') tags
  pure tags

main : IO ()
main = do
  tags <- getTags
  when (isCons tags) $ do
    summaries <- getElementsByClassName "summary"
    toShow <- join <$> traverse getElementsByClassName (("tag-" ++) <$> tags)
    traverse_ hideNode summaries
    traverse_ showNode toShow

