/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["public/**/*"],
  theme: {
    colors: {
        transparent: 'transparent',
        current: 'currentColor',
        darkest: '#1d2021',
        darker: '#282828',
        dark: '#3c3836',
        lightest: '#ebdbb2',
        lighter: '#d5c4a1',
        green: '#98971a',
        gray: '#928374',
        blue: '#83a598',
        aqua: '#8ec07c',
        purple: '#b16286',
    },
    extend: {},
  },
  plugins: [],
}
