class IdrisError extends Error { }

function __prim_js2idris_array(x){
  let acc = { h:0 };

  for (let i = x.length-1; i>=0; i--) {
      acc = { a1:x[i], a2:acc };
  }
  return acc;
}

function __prim_idris2js_array(x){
  const result = Array();
  while (x.h === undefined) {
    result.push(x.a1); x = x.a2;
  }
  return result;
}

function __lazy(thunk) {
  let res;
  return function () {
    if (thunk === undefined) return res;
    res = thunk();
    thunk = undefined;
    return res;
  };
};

function __prim_stringIteratorNew(_str) {
  return 0
}

function __prim_stringIteratorToString(_, str, it, f) {
  return f(str.slice(it))
}

function __prim_stringIteratorNext(str, it) {
  if (it >= str.length)
    return {h: 0};
  else
    return {a1: str.charAt(it), a2: it + 1};
}

function __tailRec(f,ini) {
  let obj = ini;
  while(true){
    switch(obj.h){
      case 0: return obj.a1;
      default: obj = f(obj);
    }
  }
}

const _idrisworld = Symbol('idrisworld')

const _crashExp = x=>{throw new IdrisError(x)}

const _bigIntOfString = s=> {
  try {
    const idx = s.indexOf('.')
    return idx === -1 ? BigInt(s) : BigInt(s.slice(0, idx))
  } catch (e) { return 0n }
}

const _numberOfString = s=> {
  try {
    const res = Number(s);
    return isNaN(res) ? 0 : res;
  } catch (e) { return 0 }
}

const _intOfString = s=> Math.trunc(_numberOfString(s))

const _truncToChar = x=> String.fromCodePoint(
  (x >= 0 && x <= 55295) || (x >= 57344 && x <= 1114111) ? x : 0
)

// Int8
const _truncInt8 = x => {
  const res = x & 0xff;
  return res >= 0x80 ? res - 0x100 : res;
}

const _truncBigInt8 = x => {
  const res = Number(x & 0xffn);
  return res >= 0x80 ? res - 0x100 : res;
}

// Euclidian Division
const _div = (a,b) => {
  let q = Math.trunc(a / b)
  let r = a % b
  return r < 0 ? (b > 0 ? q - 1 : q + 1) : q
}

const _divBigInt = (a,b) => {
  let q = a / b
  let r = a % b
  return r < 0n ? (b > 0n ? q - 1n : q + 1n) : q
}

// Euclidian Modulo
const _mod = (a,b) => {
  r = a % b
  return r < 0 ? (b > 0 ? r + b : r - b) : r
}

const _modBigInt = (a,b) => {
  r = a % b
  return r < 0n ? (b > 0n ? r + b : r - b) : r
}

const _add8s = (a,b) => _truncInt8(a + b)
const _sub8s = (a,b) => _truncInt8(a - b)
const _mul8s = (a,b) => _truncInt8(a * b)
const _div8s = (a,b) => _truncInt8(_div(a,b))
const _shl8s = (a,b) => _truncInt8(a << b)
const _shr8s = (a,b) => _truncInt8(a >> b)

// Int16
const _truncInt16 = x => {
  const res = x & 0xffff;
  return res >= 0x8000 ? res - 0x10000 : res;
}

const _truncBigInt16 = x => {
  const res = Number(x & 0xffffn);
  return res >= 0x8000 ? res - 0x10000 : res;
}

const _add16s = (a,b) => _truncInt16(a + b)
const _sub16s = (a,b) => _truncInt16(a - b)
const _mul16s = (a,b) => _truncInt16(a * b)
const _div16s = (a,b) => _truncInt16(_div(a,b))
const _shl16s = (a,b) => _truncInt16(a << b)
const _shr16s = (a,b) => _truncInt16(a >> b)

//Int32
const _truncInt32 = x => x & 0xffffffff

const _truncBigInt32 = x => {
  const res = Number(x & 0xffffffffn);
  return res >= 0x80000000 ? res - 0x100000000 : res;
}

const _add32s = (a,b) => _truncInt32(a + b)
const _sub32s = (a,b) => _truncInt32(a - b)
const _div32s = (a,b) => _truncInt32(_div(a,b))

const _mul32s = (a,b) => {
  const res = a * b;
  if (res <= Number.MIN_SAFE_INTEGER || res >= Number.MAX_SAFE_INTEGER) {
    return _truncInt32((a & 0xffff) * b + (b & 0xffff) * (a & 0xffff0000))
  } else {
    return _truncInt32(res)
  }
}

//Int64
const _truncBigInt64 = x => {
  const res = x & 0xffffffffffffffffn;
  return res >= 0x8000000000000000n ? res - 0x10000000000000000n : res;
}

const _add64s = (a,b) => _truncBigInt64(a + b)
const _sub64s = (a,b) => _truncBigInt64(a - b)
const _mul64s = (a,b) => _truncBigInt64(a * b)
const _div64s = (a,b) => _truncBigInt64(_divBigInt(a,b))
const _shl64s = (a,b) => _truncBigInt64(a << b)
const _shr64s = (a,b) => _truncBigInt64(a >> b)

//Bits8
const _truncUInt8 = x => x & 0xff

const _truncUBigInt8 = x => Number(x & 0xffn)

const _add8u = (a,b) => (a + b) & 0xff
const _sub8u = (a,b) => (a - b) & 0xff
const _mul8u = (a,b) => (a * b) & 0xff
const _div8u = (a,b) => Math.trunc(a / b)
const _shl8u = (a,b) => (a << b) & 0xff
const _shr8u = (a,b) => (a >> b) & 0xff

//Bits16
const _truncUInt16 = x => x & 0xffff

const _truncUBigInt16 = x => Number(x & 0xffffn)

const _add16u = (a,b) => (a + b) & 0xffff
const _sub16u = (a,b) => (a - b) & 0xffff
const _mul16u = (a,b) => (a * b) & 0xffff
const _div16u = (a,b) => Math.trunc(a / b)
const _shl16u = (a,b) => (a << b) & 0xffff
const _shr16u = (a,b) => (a >> b) & 0xffff

//Bits32
const _truncUBigInt32 = x => Number(x & 0xffffffffn)

const _truncUInt32 = x => {
  const res = x & -1;
  return res < 0 ? res + 0x100000000 : res;
}

const _add32u = (a,b) => _truncUInt32(a + b)
const _sub32u = (a,b) => _truncUInt32(a - b)
const _mul32u = (a,b) => _truncUInt32(_mul32s(a,b))
const _div32u = (a,b) => Math.trunc(a / b)

const _shl32u = (a,b) => _truncUInt32(a << b)
const _shr32u = (a,b) => _truncUInt32(a <= 0x7fffffff ? a >> b : (b == 0 ? a : (a >> b) ^ ((-0x80000000) >> (b-1))))
const _and32u = (a,b) => _truncUInt32(a & b)
const _or32u = (a,b)  => _truncUInt32(a | b)
const _xor32u = (a,b) => _truncUInt32(a ^ b)

//Bits64
const _truncUBigInt64 = x => x & 0xffffffffffffffffn

const _add64u = (a,b) => (a + b) & 0xffffffffffffffffn
const _mul64u = (a,b) => (a * b) & 0xffffffffffffffffn
const _div64u = (a,b) => a / b
const _shl64u = (a,b) => (a << b) & 0xffffffffffffffffn
const _shr64u = (a,b) => (a >> b) & 0xffffffffffffffffn
const _sub64u = (a,b) => (a - b) & 0xffffffffffffffffn

//String
const _strReverse = x => x.split('').reverse().join('')

const _substr = (o,l,x) => x.slice(o, o + l)

const Main_prim__setDisplay = ((str, el) => el.style.display = str);
const Main_prim__getQP = ((nothing, just, key) => {
  const url = new URL(document.location.href);
  const val = url.searchParams.get(key);
  if (val) {
    return just(val);
  }
  return nothing;
});
const Main_prim__getElementsByClassName = ((key) => {
  const htmlCol = document.getElementsByClassName(key);
  const htmlArr = [...htmlCol];
  return __prim_js2idris_array(htmlArr);
});
const Prelude_Types_fastUnpack = ((str)=>__prim_js2idris_array(Array.from(str)));
const Prelude_Types_fastPack = ((xs)=>__prim_idris2js_array(xs).join(''));
/* {$tcOpt:1} */
function x24tcOpt_1($0) {
 switch($0.a3.h) {
  case 0: /* nil */ return {h: 0 /* {TcDone:1} */, a1: $0.a2};
  case undefined: /* cons */ return {h: 1 /* {TcContinue1:1} */, a1: $0.a1, a2: $0.a1($0.a2)($0.a3.a1), a3: $0.a3.a2};
 }
}

/* Prelude.Types.foldl */
function Prelude_Types_foldl_Foldable_List($0, $1, $2) {
 return __tailRec(x24tcOpt_1, {h: 1 /* {TcContinue1:1} */, a1: $0, a2: $1, a3: $2});
}

/* {$tcOpt:2} */
function x24tcOpt_2($0) {
 switch($0.a3.h) {
  case 0: /* nil */ return {h: 0 /* {TcDone:2} */, a1: Prelude_Types_List_reverse($0.a2)};
  case undefined: /* cons */ return {h: 1 /* {TcContinue2:1} */, a1: $0.a1, a2: Prelude_Types_List_reverseOnto($0.a2, $0.a1($0.a3.a1)), a3: $0.a3.a2};
 }
}

/* Prelude.Types.listBindOnto : (a -> List b) -> List b -> List a -> List b */
function Prelude_Types_listBindOnto($0, $1, $2) {
 return __tailRec(x24tcOpt_2, {h: 1 /* {TcContinue2:1} */, a1: $0, a2: $1, a3: $2});
}

/* {$tcOpt:3} */
function x24tcOpt_3($0) {
 switch($0.a2.h) {
  case 0: /* nil */ return {h: 0 /* {TcDone:3} */, a1: $0.a1};
  case undefined: /* cons */ return {h: 1 /* {TcContinue3:1} */, a1: {a1: $0.a2.a1, a2: $0.a1}, a2: $0.a2.a2};
 }
}

/* Prelude.Types.List.reverseOnto : List a -> List a -> List a */
function Prelude_Types_List_reverseOnto($0, $1) {
 return __tailRec(x24tcOpt_3, {h: 1 /* {TcContinue3:1} */, a1: $0, a2: $1});
}

/* {$tcOpt:4} */
function x24tcOpt_4($0) {
 switch($0.a2.h) {
  case 0: /* nil */ return {h: 0 /* {TcDone:4} */, a1: {h: 0}};
  case undefined: /* cons */ {
   switch($0.a1($0.a2.a1)) {
    case 1: return {h: 0 /* {TcDone:4} */, a1: {a1: $0.a2.a1, a2: Prelude_Types_List_filter($0.a1, $0.a2.a2)}};
    case 0: return {h: 1 /* {TcContinue4:1} */, a1: $0.a1, a2: $0.a2.a2};
   }
  }
 }
}

/* Prelude.Types.List.filter : (a -> Bool) -> List a -> List a */
function Prelude_Types_List_filter($0, $1) {
 return __tailRec(x24tcOpt_4, {h: 1 /* {TcContinue4:1} */, a1: $0, a2: $1});
}

/* {__mainExpression:0} */
const __mainExpression_0 = __lazy(function () {
 return PrimIO_unsafePerformIO(Main_main());
});

/* {csegen:6} */
const csegen_6 = __lazy(function () {
 return {a1: b => a => func => $1 => Prelude_IO_map_Functor_IO(func, $1), a2: a => $6 => $7 => $6, a3: b => a => $9 => $a => PrimIO_io_bind($9, fx27 => PrimIO_io_bind($a, ax27 => $11 => fx27(ax27)))};
});

/* {csegen:10} */
const csegen_10 = __lazy(function () {
 return $0 => $1 => $2 => $3 => Prelude_IO_map_Functor_IO($2, $3);
});

/* {csegen:14} */
const csegen_14 = __lazy(function () {
 return $0 => $1 => $2 => $3 => Prelude_Types_map_Functor_List($2, $3);
});

/* {csegen:35} */
const csegen_35 = __lazy(function () {
 return {a1: acc => elem => func => init => input => Prelude_Types_foldr_Foldable_List(func, init, input), a2: elem => acc => func => init => input => Prelude_Types_foldl_Foldable_List(func, init, input), a3: elem => $b => Prelude_Types_null_Foldable_List($b), a4: elem => acc => m => $f => funcM => init => input => Prelude_Types_foldlM_Foldable_List($f, funcM, init, input), a5: elem => $16 => $16, a6: a => m => $18 => f => $19 => Prelude_Types_foldMap_Foldable_List($18, f, $19)};
});

/* {csegen:65} */
const csegen_65 = __lazy(function () {
 return $0 => $1 => ({a1: $0, a2: $1});
});

/* prim__add_Integer : Integer -> Integer -> Integer */
function prim__add_Integer($0, $1) {
 return ($0+$1);
}

/* prim__sub_Integer : Integer -> Integer -> Integer */
function prim__sub_Integer($0, $1) {
 return ($0-$1);
}

/* prim__mul_Integer : Integer -> Integer -> Integer */
function prim__mul_Integer($0, $1) {
 return ($0*$1);
}

/* Main.showNode : HTMLNode -> IO () */
function Main_showNode($0, $1) {
 return Main_setDisplay('', $0, $1);
}

/* Main.setDisplay : String -> HTMLNode -> IO () */
function Main_setDisplay($0, $1, $2) {
 return Main_prim__setDisplay($0, $1, $2);
}

/* Main.main : IO () */
const Main_main = __lazy(function () {
 return PrimIO_io_bind(Main_getTags(), tags => Prelude_Interfaces_when(csegen_6(), Data_List_isCons(tags), () => PrimIO_io_bind($d => Main_getElementsByClassName('summary', $d), summaries => PrimIO_io_bind(Prelude_Interfaces_x3cx24x3e(csegen_10(), $18 => Prelude_Types_join_Monad_List($18), Prelude_Types_traverse_Traversable_List(csegen_6(), $20 => $21 => Main_getElementsByClassName($20, $21), Prelude_Interfaces_x3cx24x3e(csegen_14(), $2a => ('tag-'+$2a), tags))), toShow => Prelude_Interfaces_x3ex3e({a1: csegen_6(), a2: b => a => $34 => $35 => PrimIO_io_bind($34, $35), a3: a => $3a => PrimIO_io_bind($3a, $3e => $3e)}, Prelude_Interfaces_traverse_(csegen_6(), csegen_35(), $46 => $47 => Main_hideNode($46, $47), summaries), () => Prelude_Interfaces_traverse_(csegen_6(), csegen_35(), $53 => $54 => Main_showNode($53, $54), toShow))))));
});

/* Main.hideNode : HTMLNode -> IO () */
function Main_hideNode($0, $1) {
 return Main_setDisplay('none', $0, $1);
}

/* Main.getTags : IO (List String) */
const Main_getTags = __lazy(function () {
 const $4 = tags => {
  const $5 = Prelude_Types_List_filter($8 => Prelude_EqOrd_x2fx3d_Eq_String($8, ''), Data_List1_forget(Data_String_split($11 => Prelude_EqOrd_x3dx3d_Eq_Char($11, ','), tags)));
  return $16 => $5;
 };
 return PrimIO_io_bind(Main_getQP('tags'), $4);
});

/* Main.getQP : String -> IO String */
function Main_getQP($0) {
 const $3 = csegen_10();
 const $2 = $5 => $6 => $3(undefined)(undefined)($5)($6);
 const $1 = $2($10 => Prelude_Interfaces_concat({a1: $14 => $15 => ($14+$15), a2: ''}, {a1: acc => elem => func => init => input => Prelude_Types_foldr_Foldable_Maybe(func, init, input), a2: elem => acc => func => init => input => Prelude_Types_foldl_Foldable_Maybe(func, init, input), a3: elem => $25 => Prelude_Types_null_Foldable_Maybe($25), a4: elem => acc => m => $29 => funcM => init => input => Prelude_Types_foldlM_Foldable_Maybe($29, funcM, init, input), a5: elem => $30 => Prelude_Types_toList_Foldable_Maybe($30), a6: a => m => $34 => f => $35 => Prelude_Types_foldMap_Foldable_Maybe($34, f, $35)}, $10));
 return $1($3c => Main_prim__getQP({h: 0}, $40 => ({a1: $40}), $0, $3c));
}

/* Main.getElementsByClassName : String -> IO (List HTMLNode) */
function Main_getElementsByClassName($0, $1) {
 return Main_prim__getElementsByClassName($0, $1);
}

/* Data.String.split : (Char -> Bool) -> String -> List1 String */
function Data_String_split($0, $1) {
 return Data_List1_map_Functor_List1($4 => Prelude_Types_fastPack($4), Data_List_split($0, Prelude_Types_fastUnpack($1)));
}

/* Prelude.Basics.flip : (a -> b -> c) -> b -> a -> c */
function Prelude_Basics_flip($0, $1, $2) {
 return $0($2)($1);
}

/* Builtin.believe_me : a -> b */
function Builtin_believe_me($0) {
 return $0;
}

/* Prelude.Types.traverse */
function Prelude_Types_traverse_Traversable_List($0, $1, $2) {
 switch($2.h) {
  case 0: /* nil */ return $0.a2(undefined)({h: 0});
  case undefined: /* cons */ return $0.a3(undefined)(undefined)($0.a3(undefined)(undefined)($0.a2(undefined)(csegen_65()))($1($2.a1)))(Prelude_Types_traverse_Traversable_List($0, $1, $2.a2));
 }
}

/* Prelude.Types.toList */
function Prelude_Types_toList_Foldable_Maybe($0) {
 return Prelude_Types_foldr_Foldable_Maybe(csegen_65(), {h: 0}, $0);
}

/* Prelude.Types.null */
function Prelude_Types_null_Foldable_Maybe($0) {
 switch($0.h) {
  case 0: /* nothing */ return 1;
  case undefined: /* just */ return 0;
 }
}

/* Prelude.Types.null */
function Prelude_Types_null_Foldable_List($0) {
 switch($0.h) {
  case 0: /* nil */ return 1;
  case undefined: /* cons */ return 0;
 }
}

/* Prelude.Types.map */
function Prelude_Types_map_Functor_List($0, $1) {
 switch($1.h) {
  case 0: /* nil */ return {h: 0};
  case undefined: /* cons */ {
   const $9 = b => a => func => $a => Prelude_Types_map_Functor_List(func, $a);
   const $8 = $e => $f => $9(undefined)(undefined)($e)($f);
   const $7 = $8($0);
   const $6 = $7($1.a2);
   return {a1: $0($1.a1), a2: $6};
  }
 }
}

/* Prelude.Types.join */
function Prelude_Types_join_Monad_List($0) {
 return Prelude_Types_listBind($0, $4 => $4);
}

/* Prelude.Types.foldr */
function Prelude_Types_foldr_Foldable_Maybe($0, $1, $2) {
 switch($2.h) {
  case 0: /* nothing */ return $1;
  case undefined: /* just */ return $0($2.a1)($1);
 }
}

/* Prelude.Types.foldr */
function Prelude_Types_foldr_Foldable_List($0, $1, $2) {
 switch($2.h) {
  case 0: /* nil */ return $1;
  case undefined: /* cons */ return $0($2.a1)(Prelude_Types_foldr_Foldable_List($0, $1, $2.a2));
 }
}

/* Prelude.Types.foldl */
function Prelude_Types_foldl_Foldable_Maybe($0, $1, $2) {
 return Prelude_Types_foldr_Foldable_Maybe($6 => $7 => Prelude_Basics_flip($a => $b => $c => $a($b($c)), $12 => Prelude_Basics_flip($0, $6, $12), $7), $19 => $19, $2)($1);
}

/* Prelude.Types.foldlM */
function Prelude_Types_foldlM_Foldable_Maybe($0, $1, $2, $3) {
 return Prelude_Types_foldl_Foldable_Maybe(ma => b => $0.a2(undefined)(undefined)(ma)($f => Prelude_Basics_flip($1, b, $f)), $0.a1.a2(undefined)($2), $3);
}

/* Prelude.Types.foldlM */
function Prelude_Types_foldlM_Foldable_List($0, $1, $2, $3) {
 return Prelude_Types_foldl_Foldable_List(ma => b => $0.a2(undefined)(undefined)(ma)($f => Prelude_Basics_flip($1, b, $f)), $0.a1.a2(undefined)($2), $3);
}

/* Prelude.Types.foldMap */
function Prelude_Types_foldMap_Foldable_Maybe($0, $1, $2) {
 const $4 = $5 => {
  const $7 = $0.a1;
  const $6 = $9 => $a => $7($9)($a);
  return $6($1($5));
 };
 return Prelude_Types_foldr_Foldable_Maybe($4, $0.a2, $2);
}

/* Prelude.Types.foldMap */
function Prelude_Types_foldMap_Foldable_List($0, $1, $2) {
 const $4 = acc => elem => {
  const $7 = $0.a1;
  const $6 = $9 => $a => $7($9)($a);
  const $5 = $6(acc);
  return $5($1(elem));
 };
 return Prelude_Types_foldl_Foldable_List($4, $0.a2, $2);
}

/* Prelude.Types.List.reverse : List a -> List a */
function Prelude_Types_List_reverse($0) {
 return Prelude_Types_List_reverseOnto({h: 0}, $0);
}

/* Prelude.Types.prim__integerToNat : Integer -> Nat */
function Prelude_Types_prim__integerToNat($0) {
 let $1;
 switch(((0n<=$0)?1:0)) {
  case 0: {
   $1 = 0;
   break;
  }
  default: $1 = 1;
 }
 switch($1) {
  case 1: return Builtin_believe_me($0);
  case 0: return 0n;
 }
}

/* Prelude.Types.listBind : List a -> (a -> List b) -> List b */
function Prelude_Types_listBind($0, $1) {
 return Prelude_Types_listBindOnto($1, {h: 0}, $0);
}

/* Prelude.EqOrd.== */
function Prelude_EqOrd_x3dx3d_Eq_String($0, $1) {
 switch((($0===$1)?1:0)) {
  case 0: return 0;
  default: return 1;
 }
}

/* Prelude.EqOrd.== */
function Prelude_EqOrd_x3dx3d_Eq_Char($0, $1) {
 switch((($0===$1)?1:0)) {
  case 0: return 0;
  default: return 1;
 }
}

/* Prelude.EqOrd./= */
function Prelude_EqOrd_x2fx3d_Eq_String($0, $1) {
 switch(Prelude_EqOrd_x3dx3d_Eq_String($0, $1)) {
  case 1: return 0;
  case 0: return 1;
 }
}

/* Prelude.Interfaces.when : Applicative f => Bool -> Lazy (f ()) -> f () */
function Prelude_Interfaces_when($0, $1, $2) {
 switch($1) {
  case 1: return $2();
  case 0: return $0.a2(undefined)(undefined);
 }
}

/* Prelude.Interfaces.traverse_ : Applicative f => Foldable t => (a -> f b) -> t a -> f () */
function Prelude_Interfaces_traverse_($0, $1, $2, $3) {
 return $1.a1(undefined)(undefined)($d => $e => Prelude_Interfaces_x2ax3e($0, $2($d), $e))($0.a2(undefined)(undefined))($3);
}

/* Prelude.Interfaces.concat : Monoid a => Foldable t => t a -> a */
function Prelude_Interfaces_concat($0, $1, $2) {
 return $1.a6(undefined)(undefined)($0)($d => $d)($2);
}

/* Prelude.Interfaces.>> : Monad m => m () -> Lazy (m b) -> m b */
function Prelude_Interfaces_x3ex3e($0, $1, $2) {
 return $0.a2(undefined)(undefined)($1)($c => $2());
}

/* Prelude.Interfaces.<$> : Functor f => (a -> b) -> f a -> f b */
function Prelude_Interfaces_x3cx24x3e($0, $1, $2) {
 const $5 = $0;
 const $4 = $6 => $7 => $5(undefined)(undefined)($6)($7);
 const $3 = $4($1);
 return $3($2);
}

/* Prelude.Interfaces.*> : Applicative f => f a -> f b -> f b */
function Prelude_Interfaces_x2ax3e($0, $1, $2) {
 const $d = $0.a1;
 const $c = $f => $10 => $d(undefined)(undefined)($f)($10);
 const $b = $c($1a => $1b => $1b);
 const $a = $b($1);
 const $4 = $0.a3(undefined)(undefined)($a);
 return $4($2);
}

/* Prelude.IO.map */
function Prelude_IO_map_Functor_IO($0, $1) {
 return PrimIO_io_bind($1, $5 => $6 => $0($5));
}

/* PrimIO.case block in io_bind */
function PrimIO_case__io_bind_933($0, $1) {
 const $2 = $1;
 const $3 = $0($2);
 return $3(undefined);
}

/* PrimIO.unsafePerformIO : IO a -> a */
function PrimIO_unsafePerformIO($0) {
 const $1 = $0;
 const $3 = w => {
  const $4 = $1(w);
  return $4;
 };
 return PrimIO_unsafeCreateWorld($3);
}

/* PrimIO.unsafeCreateWorld : (1 _ : ((1 _ : %World) -> a)) -> a */
function PrimIO_unsafeCreateWorld($0) {
 return $0(_idrisworld);
}

/* PrimIO.io_bind : (1 _ : IO a) -> (1 _ : (a -> IO b)) -> IO b */
function PrimIO_io_bind($0, $1) {
 const $2 = $0;
 return w => PrimIO_case__io_bind_933($1, $2(w));
}

/* Data.List1.map */
function Data_List1_map_Functor_List1($0, $1) {
 const $9 = csegen_14();
 const $8 = $b => $c => $9(undefined)(undefined)($b)($c);
 const $7 = $8($0);
 const $6 = $7($1.a2);
 return {a1: $0($1.a1), a2: $6};
}

/* Data.List1.singleton : a -> List1 a */
function Data_List1_singleton($0) {
 return {a1: $0, a2: {h: 0}};
}

/* Data.List1.forget : List1 a -> List a */
function Data_List1_forget($0) {
 return {a1: $0.a1, a2: $0.a2};
}

/* Data.List.split : (a -> Bool) -> List a -> List1 (List a) */
function Data_List_split($0, $1) {
 const $2 = Data_List_break$($0, $1);
 switch($2.a2.h) {
  case 0: /* nil */ return Data_List1_singleton($2.a1);
  case undefined: /* cons */ return {a1: $2.a1, a2: Data_List1_forget(Data_List_split($0, $2.a2.a2))};
 }
}

/* Data.List.span : (a -> Bool) -> List a -> (List a, List a) */
function Data_List_span($0, $1) {
 switch($1.h) {
  case 0: /* nil */ return {a1: {h: 0}, a2: {h: 0}};
  case undefined: /* cons */ {
   switch($0($1.a1)) {
    case 1: {
     const $8 = Data_List_span($0, $1.a2);
     return {a1: {a1: $1.a1, a2: $8.a1}, a2: $8.a2};
    }
    case 0: return {a1: {h: 0}, a2: {a1: $1.a1, a2: $1.a2}};
   }
  }
 }
}

/* Data.List.isCons : List a -> Bool */
function Data_List_isCons($0) {
 switch($0.h) {
  case 0: /* nil */ return 0;
  default: return 1;
 }
}

/* Data.List.break : (a -> Bool) -> List a -> (List a, List a) */
function Data_List_break$($0, $1) {
 const $3 = $4 => {
  switch($0($4)) {
   case 1: return 0;
   case 0: return 1;
  }
 };
 return Data_List_span($3, $1);
}


try{__mainExpression_0()}catch(e){if(e instanceof IdrisError){console.log('ERROR: ' + e.message)}else{throw e} }
